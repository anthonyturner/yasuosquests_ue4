// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "EnemyHealthWidget.h"

void UEnemyHealthWidget::SetDisplayHealth(float p_health) {

	health += p_health;
}

float UEnemyHealthWidget::GetDisplayHealth() {

	return health;
}



