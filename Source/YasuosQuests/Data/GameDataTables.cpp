// Fill out your copyright notice in the Description page of Project Settings.

#include "YasuosQuests.h"
#include "GameDataTables.h"


// Sets default values
AGameDataTables::AGameDataTables()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//DataTable'/Game/DataTables/WeaponDataTable.WeaponDataTable'
	WeaponDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), NULL, TEXT("DataTable'/Game/DataTables/WeaponDataTable.WeaponDataTable'")));
	if (WeaponDataTable == NULL) { UE_LOG(LogTemp, Error, TEXT("WeaponDataTable class datatable not found!")); }

	SpellDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), NULL, TEXT("DataTable'/Game/SpellAndDamageSystem/Examples/Data/SpellInfoData.SpellInfoData'")));
	if (SpellDataTable == NULL) { UE_LOG(LogTemp, Error, TEXT("SpellDataTable class datatable not found!")); }
	

}

// Called when the game starts or when spawned
void AGameDataTables::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGameDataTables::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

FWeaponInfoStruct*  AGameDataTables::GetWeapon(FName name) {

	return WeaponDataTable->FindRow<FWeaponInfoStruct>(name, TEXT("LookupCharacterClass"));	
}

FSpellsData* AGameDataTables::GetSpell(FName name) {

	return SpellDataTable->FindRow<FSpellsData>(name, TEXT("LookupCharacterClass"));
}

int AGameDataTables::GetWeaponTableSize() {

	int size = 0;
	if (WeaponsTable) {
		TArray<FName> weaponsTableRowsNames = WeaponsTable->GetRowNames();
		size = weaponsTableRowsNames.Num();
	}
	
	return size;
}

//Get all the data from the tables, store them inside  public structs, and print to screen
void AGameDataTables::OnFetchAllTables()
{
	//Any will be okay, not neccessarly to be this cell name
	static const FString ContextString(TEXT("Name"));

	//Get all the row names and store them temporary here, the point is to define the amount of rows, the best way yet!
	TArray<FName> weaponsTableRowsNames = WeaponsTable->GetRowNames();

	//usually we used 0 as the start index, but a table have it' first row indexed as 1, other wise it will crash
	for (int32 i = 1; i < weaponsTableRowsNames.Num() + 1; i++)
	{
		FString IndexString = FString::FromInt((int32)i);
		FName IndexName = FName(*IndexString);

		FWeaponInfoStruct* aStructRow = WeaponsTable->FindRow<FWeaponInfoStruct>(IndexName, ContextString, true);
		AllWeaponsData.Add(aStructRow);
	}
	//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, FString::FromInt(AllWeaponsData.Num()));

	//Get all the row names and stor them temporary here, the point is to define the amount of rows, the best way yet!
	TArray<FName> missionsTableRowsNames = MissionsTable->GetRowNames();

	//usually we used 0 as the start index, but a table have it' first row indexed as 1, other wise it will crash
	for (int32 e = 1; e < missionsTableRowsNames.Num() + 1; e++)
	{
		FString IndexString = FString::FromInt((int32)e);
		FName IndexName = FName(*IndexString);

		FMissionStruct* aStructRow = MissionsTable->FindRow<FMissionStruct>(IndexName, ContextString, true);
		AllMissionsData.Add(aStructRow);
	}
	//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, FString::FromInt(AllMissionsData.Num()));

	//Just a print to screen to check if I got all the values correctly!
	for (int32 x = 0; x < AllMissionsData.Num(); x++)
	{
		FString message = TEXT(" Number: ") + FString::FromInt(x) + TEXT(" Kills: ") + FString::FromInt(AllMissionsData[x]->Kill) + TEXT(" Collects: ") + FString::FromInt(AllMissionsData[x]->Collect);
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Green, message);
	}
}



