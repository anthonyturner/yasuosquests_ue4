// Copyright 2017 @Anthony Turner

#pragma once

#include "Interactables/BaseInteractables.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"
#include "BaseNPC.generated.h"

USTRUCT(Blueprintable)
struct FNPCInfoStruct
{
	GENERATED_USTRUCT_BODY()

public:


	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
	//EItemType itemType;
	// This is the NPC's name
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NpcAttributes)
		FString NpcName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NpcAttributes)
		UTexture2D* NpcImage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NpcAttributes)
		FString NpcMessage;

	//default constructor
	FNPCInfoStruct()
	{


	}
};

UCLASS()
class YASUOSQUESTS_API ABaseNPC : public ABaseInteractables
{
	GENERATED_BODY()


public:
	// Sets default values for this character's properties
	ABaseNPC();

	virtual void RegisterInteraction(IIInteractable* interact) override;
	virtual void UnRegisterInteraction(IIInteractable* interact) override;
	virtual void Interact(APawn* instig) override;

	void ShowMessage(AYasuosQuestsCharacter* player);


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NpcAttributes)
	FNPCInfoStruct npcInfo;

};


