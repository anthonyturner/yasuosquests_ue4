// Copyright 2017 @Anthony Turner

#pragma once

#include "Interactables/BaseItem.h"
#include "Interfaces/IPickup.h"
#include "PickupItem.generated.h"

UCLASS()
class YASUOSQUESTS_API APickupItem : public ABaseItem, public IIPickup
{
	GENERATED_BODY()
	
	
	public:

		APickupItem();
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pickups)
		UPointLightComponent* pickupLight;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pickups)
		URotatingMovementComponent* pickupRotationComp;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pickups)
			UTimelineComponent*	pickupTimeLineComp;


		virtual void Pickup(AYasuosQuestsCharacter* player) override;
		void DestroyWorldComponents();
};
