// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "BaseWeapon.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"


ABaseWeapon::ABaseWeapon() {

	itemInfo.itemType = EItemType::Weapon;
	
}


void ABaseWeapon::BeginPlay() {

	Super::BeginPlay();

}

void ABaseWeapon::Attack() {

	if (weaponInfo.AttackSFX)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), weaponInfo.AttackSFX, GetActorLocation());

	if(weaponInfo.weaponAnimation)
		interactableMesh->PlayAnimation(weaponInfo.weaponAnimation, false);

}


void ABaseWeapon::ResetAttack() {


}