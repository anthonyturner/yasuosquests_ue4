// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "BaseProjectile.h"




ABaseProjectile::ABaseProjectile() {


	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(25.0f);
	CollisionComp->AlwaysLoadOnClient = true;
	CollisionComp->bTraceComplexOnMove = true;
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComp->SetCollisionObjectType(ECollisionChannel::ECC_Destructible);
	CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
	CollisionComp->SetupAttachment(interactableMesh);


	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>( TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 4000.f;
	ProjectileMovement->MaxSpeed = 8000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;
	ProjectileMovement->Bounciness = 0.3f;
	
	

}

void ABaseProjectile::PostInitializeComponents()
{
	Super::PostInitializeComponents();


	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &ABaseProjectile::OnComponentBeginOverlap);

	//ProjectileMovement->OnProjectileStop.AddDynamic(this, &ABaseProjectile::OnImpact);
	CollisionComp->MoveIgnoreActors.Add(Instigator);
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, "PostInitializeComponents");

	//ABaseProjectile* OwnerWeapon = Cast<ABaseProjectile>(GetOwner());
//	if (OwnerWeapon)
	//{
	//	OwnerWeapon->ApplyWeaponConfig(WeaponConfig);
//	}

	SetLifeSpan(5000.f);
}

void ABaseProjectile::InitVelocity(const FVector& ShootDirection)
{
	if (ProjectileMovement)
	{
	
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, "InitVelocity");
		// set the projectile's velocity to the desired direction
		ProjectileMovement->Velocity = ShootDirection * ProjectileMovement->InitialSpeed;
	}
}


void ABaseProjectile::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult){

	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, "Collision impact");
	if (!bExploded){

		Explode(OtherActor);
	}
}


void ABaseProjectile::Explode(AActor* OtherActor)
{
	if (!bExploded && projectileInfo.HitVFX)
	{

		UWorld* world = GetWorld();
		UGameplayStatics::SpawnEmitterAtLocation(world, projectileInfo.HitVFX, OtherActor->GetActorLocation());

		if (projectileInfo.HitSFX) {
			UGameplayStatics::PlaySoundAtLocation(world, projectileInfo.HitSFX, OtherActor->GetActorLocation());
		}

		Destroy();
	}

	bExploded = true;
}
