// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "BaseItem.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"


ABaseItem::ABaseItem() {

}

void ABaseItem::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	isShowingItemInfo = false;
	
	if (InteractableInfoWidgetClass) {
		interactableInfoWidget = CreateWidget<UInteractableInfoWidget>(GetWorld(), InteractableInfoWidgetClass);
	}


}

void ABaseItem::BeginPlay() {

	Super::BeginPlay();
}

// Add default functionality here for any IIInteractable functions that are not pure virtual.
void ABaseItem::RegisterInteraction(IIInteractable* interact) {

	
}

void ABaseItem::UnRegisterInteraction(IIInteractable* interact) {

}


void ABaseItem::Interact(APawn* instig) {
	
	//Default behaviour for how BaseItem reacts
	AYasuosQuestsCharacter* player = Cast<AYasuosQuestsCharacter>(instig);
	if (player) {

		ToggleItemInfo(player);
	}
}

void ABaseItem::ToggleItemInfo(AYasuosQuestsCharacter* player) {

	if (interactableInfoWidget) {

		if (isShowingItemInfo) {
			
			HideItemInfo();
		} else {

			ShowItemInfo();
		}
	} 
}

void ABaseItem::ShowItemInfo() {

	interactableInfoWidget->item = this;
	interactableInfoWidget->AddToViewport();
	isShowingItemInfo = true;

}

void ABaseItem::HideItemInfo() {

	interactableInfoWidget->RemoveFromViewport();
	isShowingItemInfo = false;

}

void ABaseItem::UseItem(FItemInfoStruct itemsInfo) {


}