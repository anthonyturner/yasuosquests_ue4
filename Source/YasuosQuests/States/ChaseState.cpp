// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "ChaseState.h"
#include "WaitState.h"
#include "AttackState.h"
#include "PatrolState.h"
#include "InspectState.h"

// Sets default values
AChaseState::AChaseState()
{
	
}


void AChaseState::enter(ABaseEnemy* enemy) {

	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, "Chase state");
	enemy->Run();
	aiController = Cast<AAIController>(enemy->Controller);
	faiMR.SetAcceptanceRadius(enemy->enemyInfo.AttackRange);
}

IIEnemyState* AChaseState::Update(ABaseEnemy* enemy) {

	APawn* target = enemy->GetTarget();
	if (target) {

		float distance = FVector::Dist(target->GetActorLocation(), enemy->GetActorLocation());
		if (distance <= enemy->GetAttackRange()) {//In attack range

			return NewObject<ABaseEnemyState>(this, AAttackState::StaticClass());
		}

		

		if (aiController) {
			faiMR.SetAcceptanceRadius(enemy->enemyInfo.AttackRange);
			faiMR.SetGoalActor(target);
			aiController->MoveTo(faiMR);
		}
		return nullptr;
	} 

	return NewObject<ABaseEnemyState>(this, AInspectState::StaticClass());
}

