// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "GameFramework/Actor.h"
#include "FWeaponInfo.h"
#include "FSpellsData.h"
#include "GameDataTables.generated.h"


USTRUCT(Blueprintable)
struct FMissionStruct : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 Kill;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 Collect;

	//default constructor
	FMissionStruct()
	{
		Kill = 0;
		Collect = 0;
	}
};

UCLASS()
class YASUOSQUESTS_API AGameDataTables : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGameDataTables();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	int GetWeaponTableSize();

	//I used editanywhere, so I'll be able to assign it in the details panel
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game DataTables")
		UDataTable* WeaponsTable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game DataTables")
		UDataTable* MissionsTable;

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Game DataTables")
	TArray<FWeaponInfoStruct*> AllWeaponsData;

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Game DataTables")
	TArray<FMissionStruct*> AllMissionsData;

	UFUNCTION(BlueprintCallable, Category = "Game DataTables")
		void OnFetchAllTables();

	FWeaponInfoStruct*  GetWeapon(FName name);
	FSpellsData* GetSpell(FName name);

private:
	UDataTable* WeaponDataTable;
	UDataTable* SpellDataTable;
};
