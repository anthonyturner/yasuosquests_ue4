// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "AttackState.h"
#include "PatrolState.h"
#include "ChaseState.h"
#include "WaitState.h"
#include "InspectState.h"
#include "Kismet/KismetMathLibrary.h"

void AAttackState::enter(ABaseEnemy* enemy) {

	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, "attack state");
	enemy->Walk();
	firstAttack = true;
	//enemy->SetIsReadyToAttack()
	aiController = Cast<AAIController>(enemy->Controller);
	attackTimer = 0.f;
	//Adjust the enemy in front of the player so the enemy can attack correctly.
	
}

IIEnemyState* AAttackState::Update(ABaseEnemy* enemy) {

	APawn* target = enemy->GetTarget();
	if ( target ) {
	
			float distanceToPlayer = FVector::Dist(target->GetActorLocation(), enemy->GetActorLocation());
			if (distanceToPlayer < enemy->GetAttackRange()) {//In attack range
				
				float rand_attack_chance = FMath::RandRange(0.f, 1.f);

					AYasuosQuestsCharacter* player = Cast<AYasuosQuestsCharacter>(target);
					if (player) {
						FName attackLocationName("EnemyAttackSocket");
						attackSocketLocation = player->GetMesh()->GetSocketLocation(attackLocationName);
						faiMR.SetAcceptanceRadius(25.f);
						faiMR.SetGoalLocation(attackSocketLocation);
					}
					aiController->MoveTo(faiMR);
					GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, "MoveTo ");

					if (firstAttack) {//Used to make enemy attack when first approaches player
						enemy->PerformAttack(attackSocketLocation);
						firstAttack = false;

					} else {
							enemy->PerformAttack(attackSocketLocation);
					}
					return NewObject<ABaseEnemyState>(this, AWaitState::StaticClass());
				
			} 
	} 

	return NewObject<ABaseEnemyState>(this, AInspectState::StaticClass());
	
}

