// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "InventoryItemWidget.h"




void UInventoryItemWidget::SetItemInfo(FItemInfoStruct itemInfo) {

	itemsInfo = itemInfo;
}

FItemInfoStruct UInventoryItemWidget::GetItemInfo() {

	return itemsInfo;
}

