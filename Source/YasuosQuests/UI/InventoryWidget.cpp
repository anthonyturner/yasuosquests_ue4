// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "InventoryWidget.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"
#include "UI/InventoryItemWidget.h"



void UInventoryWidget::ClearInventory_Implementation()
{
	//add code here
	
}


const TArray<UInventoryItemWidget*> UInventoryWidget::CreateInventory() {

	TArray<UInventoryItemWidget*> inventoryWidgets;

	//static ConstructorHelpers::FClassFinder<UInventoryItemWidget>inventoryItemWidgetObj(TEXT("/Game/Blueprints/UI/UI_InventoryItem_BP"));
	if (InventoryItemWidgetClass) {
		ACharacter* character = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
		if (character) {

			AYasuosQuestsCharacter* playerCharacter = Cast<AYasuosQuestsCharacter>(character);
			if (playerCharacter) {
				
				TArray<FItemInfoStruct> inventory = playerCharacter->GetInventory();
				UWorld* world = character->GetWorld();
				int maxSize = playerCharacter->GetMaxInventorySize();

				for (int i = 0; i <  maxSize; i++) {
					FItemInfoStruct itemInfo;
					itemInfo.itemType = EItemType::None;
					
					///Create inventory widget item with item info
					UInventoryItemWidget* inventoryItemWidget = CreateWidget<UInventoryItemWidget>(world, InventoryItemWidgetClass);
					if (inventoryItemWidget) {
						if (i < inventory.Num() && inventory.IsValidIndex(i)) {

							itemInfo = inventory[i];
							
						}
						itemInfo.itemIndex = i;
						inventoryItemWidget->SetItemInfo(itemInfo);
						//add to inventory panel uniform grid
						inventoryWidgets.Add(inventoryItemWidget);
					}
				}
				//OnWidgetsCreated(inventoryWidgets);//Call blueprint event
			}
		}
	}
	
	return inventoryWidgets;
}

void UInventoryWidget::OnWidgetsCreated_Implementation(const TArray<UInventoryItemWidget*>& item) {
}

void UInventoryWidget::RefreshInventory_Implementation() {

	RefreshInventory();
}


void UInventoryWidget::UseItem(FItemInfoStruct itemInfo) {


}

void UInventoryWidget::UnUseItem(FItemInfoStruct itemInfo) {


}

void UInventoryWidget::DropItem(FItemInfoStruct itemInfo) {


}

FItemInfoStruct UInventoryWidget::GetItemInfo() {

	return selectedItemInfo;
}

void UInventoryWidget::SetItemInfo(FItemInfoStruct itemInfo) {

	selectedItemInfo = itemInfo;
}

void UInventoryWidget::EnableActionBar(FItemInfoStruct itemInfo) {

	selectedItemInfo = itemInfo;
	OnEnableActionBar(itemInfo);
}

void UInventoryWidget::OnEnableActionBar_Implementation(FItemInfoStruct itemInfo) {
}