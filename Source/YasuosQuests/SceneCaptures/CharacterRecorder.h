// Copyright 2017 @Anthony Turner

#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/IRecorderComm.h"
#include "GameModes/YQGameInstance.h"

#include "CharacterRecorder.generated.h"

class AYasuosQuestsCharacter;
UCLASS()
class YASUOSQUESTS_API ACharacterRecorder : public AActor, public IIRecorderComm
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACharacterRecorder();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ScreenCapture)
		USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=ScreenCapture)
		USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ScreenCapture)
		USceneCaptureComponent2D* SceneCaptureComponent2D;

	virtual void PassReference(ACharacterRecorder* reference) override;
	virtual void EquipItem(FItemInfoStruct item) override;
	virtual void UnEquipItem(FItemInfoStruct item) override;

	void AttachItem(FItemInfoStruct item);
	void UnAttachItem(FItemInfoStruct item);


	//-------------------------------------------------------------------------BaseItemSlots--------------------------------------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* HeadItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		USkeletalMeshComponent* SK_TorsoMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		USkeletalMeshComponent* SK_HeadMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		USkeletalMeshComponent* SK_LegsMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		USkeletalMeshComponent* SK_FeetMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* TorsoItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* RightArmItem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* LeftArmItem;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* LegsItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* FeetItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* PrimaryLeftHandItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* PrimaryRightHandItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* SecondaryLeftHandItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* SecondaryRightHandItem;

private:

	UPROPERTY()
	UYQGameInstance* gameInstance;

};
