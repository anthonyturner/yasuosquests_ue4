// Copyright 2017 @Anthony Turner

#pragma once

#include "FWeaponInfo.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EWeaponType : uint8
{
	Sword 	UMETA(DisplayName = "Sword"),
	Axe 	UMETA(DisplayName = "Axe"),
	Staff	UMETA(DisplayName = "Staff"),
	Wand	UMETA(DisplayName = "Wand"),
	Bow		UMETA(DisplayName = "Bow"),
	Dagger	UMETA(DisplayName = "Dagger")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EAttackType : uint8
{
	None 	UMETA(DisplayName = "None"),
	Melee 	UMETA(DisplayName = "Melee"),
	Range	UMETA(DisplayName = "Range"),
	Magic	UMETA(DisplayName = "Magic")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EDamageType : uint8
{
	Standard 	UMETA(DisplayName = "Standard"),
	KnockBack 	UMETA(DisplayName = "KnockBack"),
	KnockDown	UMETA(DisplayName = "KnockDown"),
	Launch	UMETA(DisplayName = "Launch")
};

USTRUCT(BlueprintType)
struct FWeaponInfoStruct : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		FName weapon_id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		FName weapon_name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		float weapon_weight;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		UClass* weaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		USkeletalMesh* weaponMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		UTexture2D* weaponImage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		EAttackType AttackType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		EDamageType DamageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		float DamageMin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		float DamageMax;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		float Range;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		float AttackSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		int level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		int weaponCost;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		EWeaponType WeaponType;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		FName PrimaryRightHand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		FName PrimaryLeftHand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		FName SecondaryRightHand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		FName SecondaryLeftHand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes, meta = (ToolTip = "Particle effct played when the weapon starts its attack"))
		UParticleSystem* AttackVFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes, meta = (ToolTip = "Sound effct played when the weapon starts its attack"))
		USoundBase* AttackSFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes, meta = (ToolTip = "particle effct played when the weapon hits something"))
		UParticleSystem* HitVFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes, meta = (ToolTip = "Sound played when the weapon hits something"))
		USoundBase* HitSFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes, meta = (ToolTip = "Weapons attack animation"))
		UAnimMontage* AttackMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		UAnimationAsset* weaponAnimation;

	//default constructor
	FWeaponInfoStruct()
	{

		AttackVFX = nullptr;
		AttackSFX = nullptr;
		HitVFX = nullptr;
		HitSFX = nullptr;
		AttackMontage = nullptr;

		PrimaryRightHand = FName(TEXT("PrimaryRightHand"));
		PrimaryLeftHand = FName(TEXT("PrimaryLeftHand"));
		SecondaryRightHand = FName(TEXT("SecondaryRightHand"));
		SecondaryLeftHand = FName(TEXT("SecondaryLeftHand"));

	}


};
