// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "PickupItem.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"

APickupItem::APickupItem() {

	pickupLight = CreateDefaultSubobject<UPointLightComponent>(TEXT("PickupLightComponent"));
	pickupLight->SetupAttachment(RootComponent);
	pickupLight->LightColor = FColor::Green;
	pickupLight->SetCastShadows(false);

	pickupRotationComp = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("pickupRotationComponent"));
	pickupRotationComp->RotationRate.Yaw = 45.f;

	
}


void APickupItem::Pickup(AYasuosQuestsCharacter* player) {
	//If player has space in inventory for pickup

	player->AddToInventory(itemInfo);
	if (isShowingItemInfo) {
		interactableInfoWidget->RemoveFromViewport();
		isShowingItemInfo = false;
	}
	Destroy();
}

void APickupItem::DestroyWorldComponents() {

	if (pickupLight)
		pickupLight->DestroyComponent();

	if (pickupRotationComp)
		pickupRotationComp->DestroyComponent();
}