// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "BaseInteractables.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"


// Sets default values
ABaseInteractables::ABaseInteractables()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//CreateDefaultSubobject<UScene>(TEXT("InteractableStaticMesh"));
	rootInteractableComponent = CreateDefaultSubobject<USceneComponent>(TEXT("rootSceneComponent"));
	RootComponent = rootInteractableComponent;

	interactableMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("InteractableStaticMesh"));
	interactableMesh->SetupAttachment(rootInteractableComponent);
	//RootComponent = interactableMesh;

	//Create interactable trigger area
	interactableArea = CreateDefaultSubobject<USphereComponent>(TEXT("interactableTriggerSphere"));
	interactableArea->SetupAttachment(rootInteractableComponent);
	interactableArea->bGenerateOverlapEvents = true;
	interactableArea->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	interactableArea->SetSphereRadius(250.f);
}




void ABaseInteractables::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	interactableArea->OnComponentBeginOverlap.AddDynamic(this, &ABaseInteractables::OnComponentBeginOverlap);
	interactableArea->OnComponentEndOverlap.AddDynamic(this, &ABaseInteractables::OnComponentEndOverlap);
	interactableArea->SetCollisionObjectType(ECollisionChannel::ECC_PhysicsBody);


}


// Called when the game starts or when spawned
void ABaseInteractables::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseInteractables::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}



// Add default functionality here for any IIInteractable functions that are not pure virtual.
void ABaseInteractables::RegisterInteraction(IIInteractable* interact) {

}

void ABaseInteractables::UnRegisterInteraction(IIInteractable* interact) {


}


void ABaseInteractables::Interact(APawn* instig){
	
}

void ABaseInteractables::SetHighlight(bool highlighted) {

	interactableMesh->SetRenderCustomDepth(highlighted);
}



void ABaseInteractables::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	
	IIInteractable* interactable = Cast<IIInteractable>(OtherActor);
	if (interactable) {

		interactable->RegisterInteraction(this);
	}
}

void ABaseInteractables::OnComponentEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {


	IIInteractable* interactable = Cast<IIInteractable>(OtherActor);
	if (interactable) {

		interactable->UnRegisterInteraction(this);
		
	}
}



