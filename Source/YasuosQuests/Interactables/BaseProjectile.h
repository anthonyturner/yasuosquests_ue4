// Copyright 2017 @Anthony Turner

#pragma once

#include "Interactables/BaseItem.h"
#include "BaseProjectile.generated.h"

USTRUCT(Blueprintable)
struct FProjectileInfoStruct
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes)
		float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes)
	float ExplosionRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes)
		float Range;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes)
		float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes)
		FName AttachPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes, meta = (ToolTip = "Particle effct played when the projectile starts its launch"))
		UParticleSystem* LaunchVFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes, meta = (ToolTip = "Sound effct played when the projectile starts its attack"))
		USoundBase* LaunchSFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes, meta = (ToolTip = "particle effct played when the projectile hits something"))
		UParticleSystem* HitVFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes, meta = (ToolTip = "Sound played when the projectile hits something"))
		USoundBase* HitSFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes, meta = (ToolTip = "projectile motion animation"))
		UAnimMontage* AttackMontage;

	//default constructor
	FProjectileInfoStruct()
	{


	}
};

UCLASS()
class YASUOSQUESTS_API ABaseProjectile : public ABaseItem
{
	GENERATED_BODY()

public:

		ABaseProjectile();

		virtual void PostInitializeComponents() override;

		/** inits velocity of the projectile in the shoot direction */
		void InitVelocity(const FVector& ShootDirection);
		
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes)
		FProjectileInfoStruct projectileInfo;
	
		/** Projectile movement component */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
			UProjectileMovementComponent* ProjectileMovement;
		
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttributes)
		USphereComponent* CollisionComp;
		
		void Explode(AActor* OtherActor);
		

		bool bExploded;

		UFUNCTION()
			void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
};
