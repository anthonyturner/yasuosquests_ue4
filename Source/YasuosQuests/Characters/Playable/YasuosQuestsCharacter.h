// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Blueprint/UserWidget.h"

#include "UI/HudWidget.h"
#include "UI/InventoryWidget.h"
#include "UI/EquipmentWidget.h"

#include "Interfaces/IInteractable.h"
#include "Interfaces/IInventory.h"
#include "Interfaces/IRecorderComm.h"

#include "Interactables/BaseInteractables.h"
#include "Interactables/BaseItem.h"
#include "Interactables/Armor/BaseArmor.h"
#include "Interactables/Weapons/BaseWeapon.h"
#include "Data/FCharacterInfo.h"

#include "Components/CastingSystemComponent.h"
#include "Data/FSpellsData.h"

#include "GameModes/YQGameInstance.h"
#include "YasuosQuestsCharacter.generated.h"



USTRUCT(Blueprintable)
struct FInteractablesDataStruct
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float dotProduct;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ABaseInteractables* interactable;


	//default constructor
	FInteractablesDataStruct()
	{
		dotProduct = 0.f;
		interactable = nullptr;

	}
};

UCLASS(config = Game)
class YASUOSQUESTS_API AYasuosQuestsCharacter : public ACharacter, public IIInteractable, public IIInventory, public IIRecorderComm
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AYasuosQuestsCharacter();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void PostInitializeComponents() override;

	virtual void RegisterInteraction(IIInteractable* interact) override;
	virtual void UnRegisterInteraction(IIInteractable* interact) override;
	virtual void Interact(APawn* instigator) override;

	void UpdateStats(float DeltaTime);
	void InitCastingSystem();
	UPROPERTY()
		UYQGameInstance* gameInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default)
		float CastingSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default)
		bool IsInControl;

	UPROPERTY()
		TArray<FCastingData> spellsList;

	FSpellsData spellOne;
	FSpellsData spellTwo;
	FSpellsData spellThree;
	FSpellsData spellFour;
	FSpellsData spellFive;


	//Casting
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		UCastingSystemComponent* CastingSystem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		FText CastingError;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Casting)
		void OnSpellInitialize(AActor* actor);

	FCastingData GetCastingData(FName name);

	//Delegates casting
	UFUNCTION(BlueprintCallable, Category = Casting)
		void OnSpellCasted(FName SpellName);

	UFUNCTION(BlueprintCallable, Category = Casting)
		void OnIntermediateEventTriggered(FName SpellName);

	UFUNCTION(BlueprintCallable, Category = Casting)
		void OnInterrupted(FName SpellName, float InteruptDuration);

	UFUNCTION(BlueprintCallable, Category = Casting)
		void OnCastStarted(FName SpellName);

	UFUNCTION(BlueprintCallable, Category = Casting)
		void OnFailedStartCast(FName SpellName, ECastFailReason reason);

	void SpellOneAbility();
	void SpellTwoAbility();
	void SpellThreeAbility();
	void SpellFourAbility();

	UFUNCTION(BlueprintCallable, Category = PlayerInfo)
		FCharacterInfo GetCharacterInfo();

	UFUNCTION(BlueprintCallable, Category = Inventory)
		virtual void UseItem(FItemInfoStruct itemInfo) override;

	UFUNCTION(BlueprintCallable, Category = Inventory)
		virtual void UnUseItem(FItemInfoStruct itemInfo) override;

	UFUNCTION(BlueprintCallable, Category = Inventory)
		virtual void DropItem(FItemInfoStruct itemInfo)override;

	UFUNCTION(BlueprintCallable, Category = Actions)
		virtual void EnableActionBar(FItemInfoStruct itemInfo)override;

	UFUNCTION(BlueprintCallable, Category = Actions)
		void DrawWeapon();

	UFUNCTION(BlueprintCallable, Category = Actions)
		void SheathWeapon();

	UFUNCTION(BlueprintCallable, Category = Actions)
		void ToggleWeapon();

	void PerformInteract();
	void PerformPickup();
	void SpawnDroppedItem(FItemInfoStruct itemInfo);

	UFUNCTION(BlueprintCallable, Category = Equipment)
		void AddToEquipment(FItemInfoStruct item);

	UFUNCTION(BlueprintCallable, Category = Equipment)
		void RemoveFromEquipment(FItemInfoStruct item);

	void RefreshEquipment();
	void UpdateEquipmentSlot(FItemInfoStruct item);
	void SpawnAttachedEquipment(ABaseItem* item, FItemInfoStruct itemInfo);

	void AttachEquipment(FItemInfoStruct item);
	void UnAttachEquipment(FItemInfoStruct item);
	void SwapItemsToSecondary(FItemInfoStruct itemInfo);

	virtual void PassReference(ACharacterRecorder* reference) override;
	virtual void EquipItem(FItemInfoStruct item) override;
	virtual void UnEquipItem(FItemInfoStruct item) override;

	UFUNCTION(BlueprintCallable, Category = Inventory)
		void AddToInventory(FItemInfoStruct item);

	UFUNCTION(BlueprintCallable, Category = Inventory)
		void RemoveFromInventory(FItemInfoStruct item);

	UFUNCTION(BlueprintCallable, Category = Inventory)
		void RefreshInventory();

	UFUNCTION(BlueprintCallable, Category = Inventory)
		bool HasSpaceInInventory();

	FInteractablesDataStruct GetBestInteractable();
	void SetBestInteractable(ABaseInteractables*);
	void HandleRegisterInteraction(ABaseInteractables* interact);
	void HandleUnRegisterInteraction(ABaseInteractables* interact);
	void UpdateBaseInteractable();

	//Camera boom positioning the camera behind the character
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
		float maxLADToInteractable;

	//Follow camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	//The sprite used to draw effect, better and more contrallable than using the HUD or Textures
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Effects, meta = (AllowPrivateAccess = "true"))
	//	class UPaperSpriteComponent* EffectSprite;


	//Base turn rate, in deg/sec. Other scaling may affect final turn rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	//Base look up/down rate, in deg/sec. Other scaling may affect final rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	//Base Jump velocity
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerAttributes)
		float jumpingVelocity;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = PlayerAttributes)
		float runSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = PlayerAttributes)
		float walkSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = PlayerAttributes)
		float attackingSpeed;

	//Is the player dead or not
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = PlayerAttributes)
		bool isAlive;

	//is the palyer attacking right now ?
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = PlayerAttributes)
		bool IsAttacking;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = PlayerAttributes)
		FName leftHandSocket;

	//is the palyer attacking right now ?
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Collectebles")
		int32 CollectedCoins;

	//is the palyer attacking right now ?
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Collectebles")
		int32 CollectedCoinsValue;

	//To be able to disable the player during cutscenes, menus, death....etc
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Collectebles")
		bool IsControlable;

	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void Attack();

	//Return if the player dead or alive
	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		bool GetIsStillAlive() const { return isAlive; }

	//Enable or disable inputs
	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		void OnSetPlayerController(bool status);

	//Perform attack
	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		void OnPlayerHit(float usedAmount);

	UFUNCTION()
		virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;


	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void OnPostHit();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Actions")
	bool isHit;

	UFUNCTION(BlueprintCallable, Category = "Player Actions")
	void Die();

	UFUNCTION()
		void HandleDie();


	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = PlayerAttributes)
		void UpdateUserInterfaceBar(FName id, float percent);


	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void OnPostAttack();

	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void OnPreAttack();

	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		float GetHealth() const;


	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		float GetMaxHealth() const;

	void SetHealth(float amount);

	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		void ChangeHealth(float amount);

	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		float GetMana() const;

	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		float GetMaxMana() const;


	void SetMana(float amount);

	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		void AddMana(float amount);

	void LevelUP();
	void AddSkillPoint(int point);
	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		void AddExp(float xp);

	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		int GetExp() const;

	UFUNCTION(BlueprintCallable, Category = PlayerAttributes)
		int GetLevel() const;

	UFUNCTION(BlueprintCallable, Category = AttackInfo)
		void ResetCombo();

	UFUNCTION(BlueprintCallable, Category = AttackInfo)
		void SaveComboAttack();

	UFUNCTION(BlueprintCallable, Category = AttackInfo)
		bool GetIsSavingAttack();

	void Walk();
	void Run();
	void ShowInventory();
	void ShowEquipment();
	//Returns CameraBoom subobject
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	//Returns FollowCamera subobject
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = PlayerAttributes)
		bool enableJumping;



	UFUNCTION(BlueprintCallable, Category = Inventory)
		TArray<FItemInfoStruct>	GetEquipmentInventory();

	UFUNCTION(BlueprintCallable, Category = Inventory)
		TArray<FItemInfoStruct> GetInventory();

	UFUNCTION(BlueprintCallable, Category = Inventory)
		UInventoryWidget* GetInventoryWidget();

	//-------------------------------------------------------------------------BaseItemSlots--------------------------------------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* HeadItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		USkeletalMeshComponent* SK_TorsoMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		USkeletalMeshComponent* SK_HeadMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		USkeletalMeshComponent* SK_ArmsMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		USkeletalMeshComponent* SK_LegsMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		USkeletalMeshComponent* SK_FeetMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* TorsoItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* RightArmItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* LeftArmItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* ArmsItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* LegsItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* FeetItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* PrimaryLeftHandItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* PrimaryRightHandItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* SecondaryLeftHandItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerItemSlots)
		ABaseItem* SecondaryRightHandItem;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAnimations)
		TArray<UAnimMontage*>attackAnims;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAnimations)
		UAnimMontage* dieAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAnimations)
		UAnimMontage* takeHitAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAnimations)
		UAnimMontage* healAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAnimations)
		UAnimMontage* Sheath_Sword_Montage;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAnimations)
		UAnimMontage* Draw_Sword_Montage;


	UFUNCTION(BlueprintCallable, Category = Inventory)
		int GetMaxInventorySize();

	UFUNCTION(BlueprintCallable, Category = WeaponInfo)
		bool HasWeaponAttached();

	UFUNCTION(BlueprintCallable, Category = WeaponInfo)
		void SetHasWeaponAttached(bool val);

	UFUNCTION(BlueprintCallable, Category = WeaponInfo)
		bool HasWeaponSheathed();

	UFUNCTION(BlueprintCallable, Category = WeaponInfo)
		void SetHasWeaponSheathed(bool val);

protected:

	void ResetCamera();
	void AdjustCamera();
	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void MoveForward(float Value);

	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void MoveRight(float Value);

	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void OnStartJump();

	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void OnStopJumping();


	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void PerformAttack();



	//Called via input to turn at a given rate.
	void TurnAtRate(float Rate);

	//Called via input to turn look up/down at a given rate.
	void LookUpAtRate(float Rate);

	//Called when we collecteing a pickup
	UFUNCTION(BlueprintCallable, Category = "PickupSystem")
		void OnCollectPickup();

	UFUNCTION(BlueprintCallable, Category = "UI")
		UHudWidget* GetHud();
	///Player stats TODO: use struct or stats classs

	//The range for the enemy attack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
		float AttackRange;


	//APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	//End of APawn interface



private:



	UPROPERTY()
		UHudWidget* hudWidget;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
		class UClass* hudWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<UInventoryWidget>InventoryWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<UEquipmentWidget>EquipmentWidgetClass;


	UPROPERTY()
		FTimerHandle AttackTimerHandle;

	UPROPERTY()
		FTimerHandle deathTimerHandle;

	UPROPERTY()
		UInventoryWidget* inventoryWidget;

	UPROPERTY()
		UEquipmentWidget* equipmentWidget;

	UPROPERTY()
		ABaseInteractables* highLightedInteractable;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
		TArray<FItemInfoStruct>inventory;

	UPROPERTY()
		TArray<ABaseInteractables*>interactables;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
		TArray<FItemInfoStruct>equipmentInventory;

	UPROPERTY()
		ACharacterRecorder* recorderReference;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float CameraAnglePower = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float CameraRotationRate = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Inventory, meta = (AllowPrivateAccess = "true"))
		USoundBase* inventoryOpenSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Inventory, meta = (AllowPrivateAccess = "true"))
		USoundBase*	inventoryCloseSound;

	UPROPERTY()
		bool isWeaponAttached;

	UPROPERTY()
		bool isWeaponSheathed;

	UPROPERTY()
		bool isInventoryOpen;

	UPROPERTY()
		bool isEquipmentOpen;

	UPROPERTY()
		bool attackButtonPressed;

	UPROPERTY()
		bool bIsAttacking;

	UPROPERTY()
		bool isSavingAttack;//Are we saving the attack for a combo

	UPROPERTY()
		int comboCounter;//Counter for current combo count

	UPROPERTY()
		int maxInventorySize;//The max capacity of the inventory

	UPROPERTY()
		float attackTimer;

	UPROPERTY()
		float forward_movement;

	UPROPERTY()
		float right_movement;

	UPROPERTY()
		FCharacterInfo characterInfo;

};


