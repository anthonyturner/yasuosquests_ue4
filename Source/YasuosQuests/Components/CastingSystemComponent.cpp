// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "CastingSystemComponent.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values for this component's properties
UCastingSystemComponent::UCastingSystemComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bReplicates = true;
	// ...

	GlobalCDDuration = 1.0f;
	MovementDetectionThreshold = 35.0f;
	TimeAfterStartCastMovementThreshold = 0.25f;
	Resource1 = 100.f;
	Resource2 = 0.f;
	MaxResource1 = 100.f;
	MaxResource2 = 1000.f;
	ManaRegenSpeedRatio = 0.05f;
}


// Called when the game starts
void UCastingSystemComponent::BeginPlay()
{
	Super::BeginPlay();
	SetIsReplicated(true);
	
	yqCharacter = Cast<AYasuosQuestsCharacter>(GetOwner());//Assuming playable character only and not enemy casting
	if (!yqCharacter) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "CastingSysteComponent: Could not get/cast to owner YQCharacter");

	}
	
}

void UCastingSystemComponent::GetLifetimeReplicatedProps(TArray< class FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(UCastingSystemComponent, AbilityButtonOneObj);
	//DOREPLIFETIME(UCastingSystemComponent, LeftHandAction);
	//DOREPLIFETIME_CONDITION(UCastingSystemComponent, AbilityList, COND_OwnerOnly);
}


// Called every frame
void UCastingSystemComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
	if (Resource1 <= MaxResource1) {
		Resource1 += DeltaTime*ManaRegenSpeedRatio;
	}

	if (yqCharacter) {

		UCharacterMovementComponent* mComp = yqCharacter->GetCharacterMovement();
		if (mComp) {

			Update(yqCharacter->CastingSpeed, yqCharacter->IsInControl, mComp->Velocity.Size(), DeltaTime);
			UpdateGlobalCD(DeltaTime);
		}
	}
}


//Adjusts global CD: if it is not active, set to -1 so it does not get replicated every frame
void UCastingSystemComponent::UpdateGlobalCD(float DeltaTime) {

	if (GetOwner()->HasAuthority()) {

		GlobalCDRemainingDuration = GlobalCDRemainingDuration - DeltaTime;
		if (GlobalCDRemainingDuration < 0.f) {

			GlobalCDRemainingDuration = -1;
		}

		if (IsCasting) {

			//Check if we just went over intermediate time event
			float newIntermediateTime = CurrentIntermediateTime + DeltaTime;
			if (newIntermediateTime <= CurrentIntermediateTime && CurrentCastData.IntermediateEventTime > CurrentIntermediateTime) {

				OnIntermediateEventTriggered.Broadcast(CurrentCastData.SpellName);
			}
			//Increment the intermediate timer
			CurrentIntermediateTime = newIntermediateTime;
			// If casting, increment the casting time
			CurrentCastTime = CurrentCastTime + DeltaTime;

			if (CurrentCastTime >= CurrentCastData.CastTime) {// Is the cast complete

				SuccessfulCast();
			} else {
				//Check if we are moving while casting
				if (!CurrentCastData.CanMoveWhileCasting) {

					//We allow some movement at the start of the cast
					if (CurrentCastTime > TimeAfterStartCastMovementThreshold) {

						//If we are moving too fast, interrupt the cast
						if (MovementSpeed > MovementDetectionThreshold) {

							InterruptCast(0.f);
						}
					}
				}
			}
			
		}
	} else {

		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Should not call TickUpdate on a non - server!");

	}
}

void UCastingSystemComponent::Update(float NewCastingSpeed, bool bIsNotInControl, float fMovementSpeed, float DeltaTime) {

	if (GetOwner()->HasAuthority()) {


		CastingSpeedRatio = NewCastingSpeed;
		IsNotInControl = bIsNotInControl;
		IsSilenced = CastingSpeedRatio <= 0; //Silence == casting speed is negative!
		MovementSpeed = fMovementSpeed;
		if (IsCasting) {

			//If we are not in control anymore, the cast is interrupted (unless it can be used while not in control)
			if (IsNotInControl && !CurrentCastData.CanBeUsedNotInControl) {

				InterruptCast(0.f);
			}

			//If we receaive a silence, cast is interrupted as well
			if (IsSilenced && !CurrentCastData.CanBeUsedWhileSilenced) {

				InterruptCast(0.f);
			}
		}	
	}

	
}


void UCastingSystemComponent::InterruptCast(float interuptDuration) {

	if (GetOwner()->HasAuthority()) {

		if (IsCasting) {
			//Stop casting and set interupt duration
			IsCasting = false;
			InterruptRemainingDuration = FMath::Clamp(interuptDuration, 0.f, 100.f);
			
			OnInterruptedCast.Broadcast(CurrentCastData.SpellName, InterruptRemainingDuration);
		}
	}

}

bool UCastingSystemComponent::StartCast(FCastingData castingData) {

	if (GetOwner()->HasAuthority()) {

		LastFailReason = TryStartCast(castingData);
		if (LastFailReason == ECastFailReason::NoReason) {
			//If succesful, set the current data and reset the casting time
			CurrentCastData = castingData;
			CurrentCastTime = 0.f;
			CurrentIntermediateTime = 0.f;
			ApplyCastingSpeedRatio();
			//We treat all casts shorter than 0.05second as instant! previously
			//We treat all casts shorter than 0.2 second as instant! current
			if (castingData.CastTime < 0.2f) {//Instant cast

				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Instant");

				OnStartedCast.Broadcast(castingData.SpellName);
				IsCasting = true;
				SuccessfulCast();
				return false;
			} else { //Not Instant cast

				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Not Instant");

				OnStartedCast.Broadcast(castingData.SpellName);
				IsCasting = true;
				return true;
			}
		} else {

			OnFailedStartCast.Broadcast(castingData.SpellName, LastFailReason);
			return false;
		}

	} else {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Should not call start cast on a non - server!");
		return false;
	}
}

ECastFailReason UCastingSystemComponent::TryStartCast(FCastingData castingData) {

	LocalCastData = castingData;
	
	if (IsCasting) {

		return ECastFailReason::AlreadyCasting;
	}

	if (!castingData.CanBeUsedWhileInterrupted && InterruptRemainingDuration > 0.f) {
		//Interrupt check
		return ECastFailReason::Interrupted;
	}

	if(GlobalCDRemainingDuration > 0.f){

		//Global CD check
		return ECastFailReason::GlobalCooldownNotAvailable;
	}

	if (!castingData.CanBeUsedWhileSilenced && IsSilenced) {
		//Silence check
		return ECastFailReason::Silenced;
	}

	if (!castingData.CanBeUsedNotInControl && IsNotInControl) {
		//Control check
		return ECastFailReason::NotInControl;
	}

	if (castingData.Resource1Use > Resource1) {
		//Resource one check
		return ECastFailReason::NotEnoughResource1;
	}

	if (castingData.Resource2Use > Resource2) {
		//Resource two check
		return ECastFailReason::NotEnoughResource2;
	}

	return ECastFailReason::NoReason;
}

//Trigger global CD; for cast longer than GlobalCDDuration, this trigerrs nothing
void UCastingSystemComponent::SuccessfulCast() {

	if (CurrentCastData.DoesNoTriggerGlobalCD) {

		//Apply the resource change
		IsCasting = false;
		ApplyResourceChange();
		OnSuccessfulCast.Broadcast(CurrentCastData.SpellName);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "SuccessfulCast calling ()");

	} else {

		TriggerGlobalCD(CurrentCastTime);
	}

}


//Update casting time based on "modified" casting speed ratio. If it can be used while silenced, casting decrease does not apply.
void UCastingSystemComponent::ApplyCastingSpeedRatio() {

	float clampedCastingSpeed = FMath::Clamp(CastingSpeedRatio, 0.f, 100.f);
	float selectFloat = CurrentCastData.CastTime *  clampedCastingSpeed;
	CurrentCastData.CastTime = UKismetMathLibrary::SelectFloat(1.0f, selectFloat, CurrentCastData.CanBeUsedWhileSilenced);

	selectFloat = CurrentCastData.IntermediateEventTime *  clampedCastingSpeed;
	CurrentCastData.IntermediateEventTime = UKismetMathLibrary::SelectFloat(1.0f, selectFloat, CurrentCastData.CanBeUsedWhileSilenced);

}


void UCastingSystemComponent::ApplyResourceChange() {

	if (GetOwner()->HasAuthority()) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "ApplyResourceChange");

		//Change the first resource
		 float newResourceVal = Resource1 - CurrentCastData.Resource1Use + CurrentCastData.Resource1Gain;
		 Resource1 = FMath::Clamp(newResourceVal, 0.f, MaxResource1);
		 GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Resource 1 value " + FString::SanitizeFloat(Resource1));

		 //Change the second resource
		 newResourceVal = Resource2 - CurrentCastData.Resource2Use + CurrentCastData.Resource2Gain;
		 Resource2 = FMath::Clamp(newResourceVal, 0.f, MaxResource2);
	} else {

		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Should not call ApplyResources on a client!");

	}
}

void UCastingSystemComponent::RevertResourceChange() {

	if (GetOwner()->HasAuthority()) {

		//Change the first resource
		float newResourceVal = Resource1 + CurrentCastData.Resource1Use - CurrentCastData.Resource1Gain;
		Resource1 = FMath::Clamp(newResourceVal, 0.f, MaxResource1);

		//Change the second resource
		newResourceVal = Resource2 + CurrentCastData.Resource2Use - CurrentCastData.Resource2Gain;
		Resource2 = FMath::Clamp(newResourceVal, 0.f, MaxResource2);
	} else {

		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Should not call RevertResource on a client!");

	}
}

//Set the global CD, reduced by the duration specified
void UCastingSystemComponent::TriggerGlobalCD(float ReducedGCDTime) {

	if (GetOwner()->HasAuthority()) {

		GlobalCDRemainingDuration =  FMath::Clamp((GlobalCDDuration - ReducedGCDTime), 0.f, GlobalCDDuration);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Reduced gcd " + FString::SanitizeFloat(GlobalCDRemainingDuration));

	}
	else {

		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Should not call TriggerGlobalCD on a client!");

	}
}

