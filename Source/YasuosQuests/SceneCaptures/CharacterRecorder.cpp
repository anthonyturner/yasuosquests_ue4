// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "CharacterRecorder.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"

// Sets default values
ACharacterRecorder::ACharacterRecorder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = Root;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMesh->SetupAttachment(RootComponent);//AttachTo


	SK_HeadMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HeadsUSkeletalMeshComponent"));
	SK_HeadMesh->SetupAttachment(SkeletalMesh);//AttachTo


	SK_TorsoMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TorsoUSkeletalMeshComponent"));
	SK_TorsoMesh->SetupAttachment(SkeletalMesh);//AttachTo

	SK_LegsMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("LegsUSkeletalMeshComponent"));
	SK_LegsMesh->SetupAttachment(SkeletalMesh);//AttachTo

	SK_FeetMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FeetUSkeletalMeshComponent"));
	SK_FeetMesh->SetupAttachment(SkeletalMesh);//AttachTo

	SceneCaptureComponent2D = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("SceneCaptureComponent2D"));
	SceneCaptureComponent2D->SetupAttachment(RootComponent);//AttachTo

}

// Called when the game starts or when spawned
// Prerequisite: Must put CharacterRecord Blueprint in game world
void ACharacterRecorder::BeginPlay()
{
	Super::BeginPlay();

	ACharacter* character = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (character) {
		AYasuosQuestsCharacter* playerCharacter = Cast<AYasuosQuestsCharacter>(character);
		if (playerCharacter) {
			playerCharacter->PassReference(this);
			gameInstance = playerCharacter->gameInstance;
		}
	}
}

// Called every frame
void ACharacterRecorder::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}


void ACharacterRecorder::PassReference(ACharacterRecorder* reference) {


}


void ACharacterRecorder::EquipItem(FItemInfoStruct itemInfo) {

	AttachItem(itemInfo);
}

void ACharacterRecorder::UnEquipItem(FItemInfoStruct itemInfo) {

	UnAttachItem(itemInfo);
}


void ACharacterRecorder::AttachItem(FItemInfoStruct itemInfo) {

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	ABaseWeapon* weapon = nullptr;
	ABaseArmor* armor = nullptr;
	FName weaponName;
	FWeaponInfoStruct* weaponInfo = nullptr;


	APickupItem* pickupItem = GetWorld()->SpawnActorDeferred<APickupItem>(itemInfo.itemClass, GetActorTransform(), this, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	pickupItem->DestroyWorldComponents();
	UGameplayStatics::FinishSpawningActor(pickupItem, pickupItem->GetActorTransform());
	switch (itemInfo.itemType) {

	case EItemType::Weapon:

		weapon = Cast<ABaseWeapon>(pickupItem);
		weaponInfo = gameInstance->FetchWeapon(itemInfo.item_id);
		weapon->weaponInfo = *weaponInfo;
		if (weapon && weaponInfo) {

			weapon->itemInfo = itemInfo;
			weapon->interactableMesh->SetSkeletalMesh(weaponInfo->weaponMesh);
			switch (itemInfo.itemSlot) {

			case  ESlotType::PrimaryLeftHand:

				if (PrimaryLeftHandItem != nullptr) {
					PrimaryLeftHandItem->Destroy();
				}
				PrimaryLeftHandItem = weapon;
				PrimaryLeftHandItem->AttachToComponent(SkeletalMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.PrimaryLeftHand);
				PrimaryLeftHandItem->SetActorEnableCollision(false);
				break;

			case ESlotType::PrimaryRightHand:
				if (PrimaryRightHandItem != nullptr) {
					PrimaryRightHandItem->Destroy();
				}
				PrimaryRightHandItem = weapon;
				PrimaryRightHandItem->AttachToComponent(SkeletalMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.PrimaryRightHand);
				PrimaryRightHandItem->SetActorEnableCollision(false);
				break;

			case  ESlotType::SecondaryLeftHand:
				if (SecondaryLeftHandItem != nullptr) {
					SecondaryLeftHandItem->Destroy();
				}
				SecondaryLeftHandItem = weapon;
				SecondaryLeftHandItem->AttachToComponent(SkeletalMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.SecondaryLeftHand);
				SecondaryLeftHandItem->SetActorEnableCollision(false);
				break;
			case  ESlotType::SecondaryRightHand:
				if (SecondaryRightHandItem != nullptr) {
					SecondaryRightHandItem->Destroy();
				}
				SecondaryRightHandItem = weapon;
				SecondaryRightHandItem->AttachToComponent(SkeletalMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.SecondaryRightHand);
				SecondaryRightHandItem->SetActorEnableCollision(false);
				break;
			}
		}
		break;
		//====================================================================================
	case EItemType::Armor:

		armor = Cast<ABaseArmor>(pickupItem);
		armor->itemInfo = itemInfo;
		switch (itemInfo.itemSlot) {

		case ESlotType::Head:

			if (HeadItem != nullptr) {
				HeadItem->Destroy();

			}
			HeadItem = armor;
			
			//SK_HeadMesh->SetSkeletalMesh(HeadItem->interactableMesh->SkeletalMesh);
			break;

		case ESlotType::Torso:

			if (TorsoItem != nullptr) {
				TorsoItem->Destroy();
			}
			TorsoItem = armor;
			SK_TorsoMesh->SetSkeletalMesh(TorsoItem->interactableMesh->SkeletalMesh);
			//item->Destroy();
			break;

		case ESlotType::Arms:
			if (RightArmItem != nullptr) {
				RightArmItem->Destroy();
			}

			if (LeftArmItem != nullptr) {
				LeftArmItem->Destroy();
			}

			RightArmItem = armor;
			RightArmItem->AttachToComponent(SkeletalMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.RightAttachPoint);
			RightArmItem->SetActorEnableCollision(false);


			LeftArmItem = armor;
			LeftArmItem->AttachToComponent(SkeletalMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.LeftAttachPoint);
			LeftArmItem->SetActorEnableCollision(false);


		case ESlotType::Legs:
			if (LegsItem != nullptr) {
				LegsItem->Destroy();
			}
			LegsItem = armor;
			SK_LegsMesh->SetSkeletalMesh(LegsItem->interactableMesh->SkeletalMesh);
			break;

		case ESlotType::Feet:
			if (FeetItem != nullptr) {
				FeetItem->Destroy();
			}
			FeetItem = armor;
			SK_FeetMesh->SetSkeletalMesh(FeetItem->interactableMesh->SkeletalMesh);
			break;
		}
		armor->SetActorHiddenInGame(true);
	}
}

void ACharacterRecorder::UnAttachItem(FItemInfoStruct itemInfo) {
	switch (itemInfo.itemSlot) {

	case ESlotType::Head:

		if (HeadItem != nullptr) {
			HeadItem->Destroy();
			SK_HeadMesh->SetSkeletalMesh(nullptr);
		}
		break;

	case ESlotType::Torso:
		if (TorsoItem != nullptr) {
			TorsoItem->Destroy();
			SK_TorsoMesh->SetSkeletalMesh(nullptr);
		}
		break;

	case ESlotType::Arms:
		if (RightArmItem != nullptr) {
			RightArmItem->Destroy();
			//	SK_ArmsMesh->SetSkeletalMesh(nullptr);

		}

		if (LeftArmItem != nullptr) {
			LeftArmItem->Destroy();
			//	SK_ArmsMesh->SetSkeletalMesh(nullptr);

		}
		break;

	case ESlotType::Legs:
		if (LegsItem != nullptr) {
			LegsItem->Destroy();

		}
		break;

	case ESlotType::Feet:
		if (FeetItem != nullptr) {
			FeetItem->Destroy();

		}
		break;

	case ESlotType::PrimaryLeftHand:
		if (PrimaryLeftHandItem != nullptr) {
			PrimaryLeftHandItem->Destroy();

		}
		break;

	case ESlotType::PrimaryRightHand:
		if (PrimaryRightHandItem != nullptr) {
			PrimaryRightHandItem->Destroy();
			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, "Primary right hand ");

		}
		else {

			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, "Primary right hand is null");

		}

		break;

	case ESlotType::SecondaryLeftHand:
		if (SecondaryLeftHandItem != nullptr) {
			SecondaryLeftHandItem->Destroy();
		}
		break;

	case ESlotType::SecondaryRightHand:
		if (SecondaryRightHandItem != nullptr) {
			SecondaryRightHandItem->Destroy();

		}
		break;

	case ESlotType::None:
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, "No equipment");
		break;
	}

}