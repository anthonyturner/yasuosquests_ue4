// Copyright 2017 @Anthony Turner

#pragma once

#include "Interactables/BaseInteractables.h"
#include "UI/InteractableInfoWidget.h"
#include "Data/FItemInfo.h"
#include "BaseItem.generated.h"

class AYasuosQuestsCharacter;
class UInteractableInfoWidget;
UCLASS()
class YASUOSQUESTS_API ABaseItem : public ABaseInteractables
{
	GENERATED_BODY()
	
	
public:

	ABaseItem();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
	FItemInfoStruct itemInfo;

	void ToggleItemInfo(AYasuosQuestsCharacter* player);
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;
	virtual void RegisterInteraction(IIInteractable* interact) override;
	virtual void UnRegisterInteraction(IIInteractable* interact) override;
	virtual void Interact(APawn* instig) override;

	virtual void UseItem(FItemInfoStruct itemInfo);
	
	void ShowItemInfo();
	void HideItemInfo();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UI)
	TSubclassOf<UInteractableInfoWidget> InteractableInfoWidgetClass;

	UPROPERTY()
	UInteractableInfoWidget* interactableInfoWidget;

protected:
	bool isShowingItemInfo;
};
