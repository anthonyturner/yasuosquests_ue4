// Copyright 2017 @Anthony Turner

#pragma once

#include "BaseEnemyState.h"
#include "WaitState.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API AWaitState : public ABaseEnemyState
{
	GENERATED_BODY()
	

public:
	// Sets default values for this actor's properties
	AWaitState();

	virtual IIEnemyState* Update(ABaseEnemy* enemy)  override;
	virtual void enter(ABaseEnemy* enemy) override;

	UPROPERTY()
	float MAX_WAIT_TIME = 16.f;
	
	UPROPERTY()
		float MIN_WAIT_TIME = 4.f;

private:
	
	float waitTime;
	
	UPROPERTY()
	UWorld* world;

	float randWaitTime;
};
