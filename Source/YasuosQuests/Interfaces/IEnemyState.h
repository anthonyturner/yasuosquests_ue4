// Copyright 2017 @Anthony Turner

#pragma once

#include "Characters/Enemies/BaseEnemy.h"
#include "IEnemyState.generated.h"


UINTERFACE(MinimalAPI)
class UIEnemyState : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class YASUOSQUESTS_API IIEnemyState
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	virtual IIEnemyState* Update(ABaseEnemy* enemy) = 0;
	virtual void enter(ABaseEnemy* enemy) = 0;
	
	
	
};
