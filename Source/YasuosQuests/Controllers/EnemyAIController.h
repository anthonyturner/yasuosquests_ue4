// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "BehaviorTree/BehaviorTree.h" 
#include "BehaviorTree/BehaviorTreeComponent.h" 
#include "BehaviorTree/BlackboardComponent.h" 
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"

#include "EnemyAIController.generated.h"


class ABaseEnemy;
UCLASS()
class YASUOSQUESTS_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
	
	virtual void Possess(class APawn* InPawn) override;  
	
	virtual void BeginInactiveState() override;
public:

	AEnemyAIController();


protected: 
	int32 EnemyKeyID;  
	int32 EnemyPositionKeyID;


	
};
