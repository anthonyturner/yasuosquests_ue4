// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "YasuosQuests.h"
#include "YasuosQuestsGameMode.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"

AYasuosQuestsGameMode::AYasuosQuestsGameMode()
{
	// set default pawn class to our Blueprinted character
	//	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/StarterContent/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));

	//Blueprint'/Game/Blueprints/Characters/Playable/BP_YasuosQuestsCharacter.BP_YasuosQuestsCharacter'
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Characters/Playable/BP_YasuosQuestsCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
