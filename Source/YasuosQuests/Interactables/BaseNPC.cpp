// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "BaseNPC.h"
#include "UI/YasuosQuestsGameHUD.h"

// Sets default values
ABaseNPC::ABaseNPC()
{

}

// Add default functionality here for any IIInteractable functions that are not pure virtual.
void ABaseNPC::RegisterInteraction(IIInteractable* interact) {




}

void ABaseNPC::UnRegisterInteraction(IIInteractable* interact) {

}


void ABaseNPC::Interact(APawn* instig) {

	//Default behaviour for how BaseItem reacts
	AYasuosQuestsCharacter* player = Cast<AYasuosQuestsCharacter>(instig);
	if (player) {

		ShowMessage(player);
	}
}

void ABaseNPC::ShowMessage(AYasuosQuestsCharacter* player)
{

	APlayerController* PController = Cast<APlayerController>(player->Controller);
	if (PController){
		
		AYasuosQuestsGameHUD* hud = Cast<AYasuosQuestsGameHUD>(PController->GetHUD());

		if (hud) {
			hud->AddMessage(Message(npcInfo.NpcName + FString(": ") + npcInfo.NpcMessage, 5.f, FColor::White, npcInfo.NpcImage));
		}
		else {


		}
	}
}


