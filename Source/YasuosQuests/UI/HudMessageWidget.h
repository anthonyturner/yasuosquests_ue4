// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "HudMessageWidget.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API UHudMessageWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = HudDisplay)
	void SetMessage(FString message);
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FString message;
	
};
