// Copyright 2017 @Anthony Turner

#pragma once
#include "Blueprint/UserWidget.h"
#include "InteractableInfoWidget.generated.h"


class ABaseItem;
UCLASS()
class YASUOSQUESTS_API UInteractableInfoWidget : public UUserWidget
{
	GENERATED_BODY()
	
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = InteractableItemInfo)
		ABaseItem* item;
};
