// Copyright 2017 @Anthony Turner

#pragma once

#include "States/BaseEnemyState.h"
#include "AttackState.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API AAttackState : public ABaseEnemyState
{
	GENERATED_BODY()
	
	virtual IIEnemyState* Update(ABaseEnemy* enemy)  override;
	virtual void enter(ABaseEnemy* enemy) override;

private:

	float attackTimer;
	UPROPERTY()
	FVector attackSocketLocation;
	
	FVector attackSocketRotation;

	FAIMoveRequest faiMR;

	bool firstAttack;

	UPROPERTY()
		AAIController* aiController;
};
