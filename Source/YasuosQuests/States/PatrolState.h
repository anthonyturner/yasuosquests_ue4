// Copyright 2017 @Anthony Turner

#pragma once

#include "BaseEnemyState.h"
#include "PatrolState.generated.h"

UCLASS()
class YASUOSQUESTS_API APatrolState : public ABaseEnemyState
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APatrolState();

	virtual IIEnemyState* Update(ABaseEnemy* enemy)  override;
	virtual void enter(ABaseEnemy* enemy) override;

	FNavLocation patrolLocation;
	
	
	UPROPERTY()
	float patrolRadius;
	
};
