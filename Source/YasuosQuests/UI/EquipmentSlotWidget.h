// Copyright 2017 @Anthony Turner

#pragma once

#include "Blueprint/UserWidget.h"
#include "Interactables/BaseItem.h"
#include "Interfaces/IInventory.h"
#include "EquipmentSlotWidget.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API UEquipmentSlotWidget : public UUserWidget, public IIInventory
{
	GENERATED_BODY()
	
	
	
public:



	UFUNCTION(BlueprintCallable, Category = UIEquipmentInvenvtoryItem)
	virtual void UseItem(FItemInfoStruct item) override;

	UFUNCTION(BlueprintCallable, Category = UIEquipmentInvenvtoryItem)
	virtual void UnUseItem(FItemInfoStruct item) override;

	UFUNCTION(BlueprintCallable, Category = UIEquipmentInvenvtoryItem)
	virtual void DropItem(FItemInfoStruct item) override;

	UFUNCTION(BlueprintCallable, Category = UIEquipmentInvenvtoryItem)
	virtual void EnableActionBar(FItemInfoStruct item) override;


	UFUNCTION(BlueprintCallable, Category = UIEquipmentInvenvtoryItem)
		FItemInfoStruct GetItemInfo();


	UFUNCTION(BlueprintCallable, Category = UIEquipmentInvenvtoryItem)
		void SetItemInfo(FItemInfoStruct item);

private:
		FItemInfoStruct ItemInfo;
};
