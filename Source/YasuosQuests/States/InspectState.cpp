// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "InspectState.h"
#include "PatrolState.h"
#include "ChaseState.h"

// Sets default values
AInspectState::AInspectState()
{

}


void AInspectState::enter(ABaseEnemy* enemy) {

	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, "InspectState state");
	enemy->Walk();
	aiController = Cast<AAIController>(enemy->Controller);
	world = enemy->GetWorld();

	currentInspectTime = 0.f;
}

IIEnemyState* AInspectState::Update(ABaseEnemy* enemy) {
	
	APawn* target = enemy->GetTarget();
	if (target) {

		return NewObject<ABaseEnemyState>(this, AChaseState::StaticClass());
	}

	float deltaSeconds = world->DeltaTimeSeconds;
	currentInspectTime += deltaSeconds;
	
	//TODO Code to make enemy turn around and look constantly here

	if (currentInspectTime >= inspectTime) {
		//Did not see anything suspicious
		return NewObject<ABaseEnemyState>(this, APatrolState::StaticClass());
	}

	return nullptr;
}

