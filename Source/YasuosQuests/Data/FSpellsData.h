#pragma once
#include "FSpellsData.generated.h"



UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ESpellDamageType : uint8
{
	Magic 	UMETA(DisplayName = "Magic"),
	Physical 	UMETA(DisplayName = "Physical"),
	TrueDamage 	UMETA(DisplayName = "TrueDamage"),
	Healing 	UMETA(DisplayName = "Healing")

};

USTRUCT(BlueprintType)
struct FSpellsData : public FTableRowBase {

	GENERATED_USTRUCT_BODY()

public:


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		FName SpellName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		ESpellDamageType SpellType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
	TSubclassOf<AActor> BP_Spell;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		float SpellStartOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		FName SpellGroup;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		float Power;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
	float CastTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		float ManaCost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		bool CanMoveWhileCasting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		bool DoesNoTriggerGlobalCD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		bool CanBeUsedNotInControl;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		float CriticalStrikeChance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		float PowerSecondary;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		float PowerVariation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		float CriticalStrikeDamageMultiplier;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		FText DisplayName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Info")
		UAnimMontage* SpellAnimation;

};
