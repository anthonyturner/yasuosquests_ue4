// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "IPickup.h"


// This function does not need to be modified.
UIPickup::UIPickup(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IIPickup functions that are not pure virtual.
