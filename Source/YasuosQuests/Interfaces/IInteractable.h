// Copyright 2017 @Anthony Turner

#pragma once

#include "IInteractable.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UIInteractable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class YASUOSQUESTS_API IIInteractable
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	virtual void RegisterInteraction(IIInteractable* interact) = 0;
	virtual void UnRegisterInteraction(IIInteractable* interact) = 0;
	//classes using this interface must implement Interact
	virtual void Interact(APawn* instigator) = 0;

	//virtual void Interact(APawn* instigator);
	virtual void SetHighlight(bool);


};
