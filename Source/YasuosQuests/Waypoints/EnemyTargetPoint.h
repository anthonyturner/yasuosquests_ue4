// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/TargetPoint.h"
#include "EnemyTargetPoint.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API AEnemyTargetPoint : public ATargetPoint
{
	GENERATED_BODY()
	
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ZoneLevel)
		int zoneLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ZoneLevel)
		int numEnemies;//Num enemies at this way point
	
};
