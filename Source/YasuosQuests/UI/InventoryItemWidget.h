// Copyright 2017 @Anthony Turner

#pragma once

#include "Blueprint/UserWidget.h"
#include "Interactables/BaseItem.h"

#include "InventoryItemWidget.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API UInventoryItemWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = InventoryItemWidget)
	void SetItemInfo(FItemInfoStruct itemInfo);

	UFUNCTION(BlueprintCallable, Category = InventoryItemWidget)
	FItemInfoStruct GetItemInfo();

private:
		FItemInfoStruct itemsInfo;
};
