// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
//#include "Blueprint/UserWidget.h"
#include "UI/PlayerHealthWidget.h"
#include "UI/HudMessageWidget.h"
#include "GameFramework/HUD.h"

#include "YasuosQuestsGameHUD.generated.h"

struct Message
{
	FString message;
	float time;
	FColor color;
	UTexture2D* tex;

	Message()
	{
		// Set the default time.
		time = 5.f;
		color = FColor::White;
	}

	Message(FString iMessage, float iTime, FColor iColor, UTexture2D* avatar)
	{
		message = iMessage;
		time = iTime;
		color = iColor;
		tex = avatar;
		
	}
};

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API AYasuosQuestsGameHUD : public AHUD
{
	GENERATED_BODY()

public:
		virtual void BeginPlay() override;
		virtual void DrawHUD() override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUDFont)
	UFont* hudFont;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PlayerHUD")
	//	TSubclassOf<UPlayerHealthWidget> HealthWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PlayerHUD")
	TSubclassOf<UHudMessageWidget> MessageWidgetClass;
	
	//UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PlayerHUD")
	//UPlayerHealthWidget* healthWidget;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PlayerHUD")
	UHudMessageWidget* messageWidget;

	void AddWidgets();
	//void SetHealth(float amount);

//	UFUNCTION()
	//void OnTakeDamage(float amount);
	void AddMessage(Message msg);

private:

	// New! An array of messages for display
	TArray<Message> messages;
	// New! A function to be able to add a message to display
	void DrawMessages();
};
