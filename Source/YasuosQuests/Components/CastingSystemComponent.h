// Copyright 2017 @Anthony Turner

#pragma once

#include "Components/ActorComponent.h"
#include "Data/FCastingData.h"
#include "CastingSystemComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FInterruptedCast, FName, SpellName, float, InteruptDuration);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSuccessfulCast, FName, SpellName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStartedCast, FName, SpellName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FIntermediateEventTriggered, FName, SpellName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FFailedStartCast, FName, SpellName, ECastFailReason, Reason);


class AYasuosQuestsCharacter;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class YASUOSQUESTS_API UCastingSystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCastingSystemComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override ;
	
	UPROPERTY()
	AYasuosQuestsCharacter* yqCharacter;
	
	//Resources	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Resource,  meta = (ToolTip = "The primary resource, like mana or rage."), Replicated)
		float Resource1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Resource, meta = (ToolTip = "The secondary resource, if required."), Replicated)
		float Resource2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Resource, meta = (ToolTip = "Maximum of the primary resource, it is clamped to [0,this value]."))
		float MaxResource1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Resource, meta = (ToolTip = "Maximum of the secondary resource, it is clamped to [0,this value]."))
		float MaxResource2;

	//Casting
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting, meta = (ToolTip = "The duration of one global CD. This is used for short/instant casts so you cannot spam them to frequently."))
		float GlobalCDDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting, meta = (ToolTip = "The amount of movement still allowed (after the time after start cast movement threshold) that does not interrupt casts that cannot be sed while moving."))
		float MovementDetectionThreshold;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting,  meta = (ToolTip = "The remaining interrupt duration."), Replicated)
		float InterruptRemainingDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting, meta = (ToolTip = "The current cast time. It is only valid when IsCasting is true"), Replicated)
		float CurrentCastTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting,  meta = (ToolTip = "The current spell being cast."), Replicated)
		FCastingData CurrentCastData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting, meta = (ToolTip = "The remaining global CD duration."), Replicated)
		float GlobalCDRemainingDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting, meta = (ToolTip = ""), Replicated)
		bool IsCasting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting, meta = (ToolTip = "The time threshold when you can move without the spell being interrupted fue to movement."))
		float TimeAfterStartCastMovementThreshold;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting, meta = (ToolTip = "The current intermediate time for event 'during' the cast.."))
		float CurrentIntermediateTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting, meta = (ToolTip = "The reason why the last cast failed"))
		ECastFailReason LastFailReason;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default, meta = (ToolTip = ""))
		FCastingData LocalCastData;

	//Updated From Actor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Updated From Actor", meta = (ToolTip = "The spell haste ratio. Use Update each frame to update it from the parent."))
		float CastingSpeedRatio;

	//Updated From Actor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Updated From Actor", meta = (ToolTip = "The spell's mana regen speeed"))
		float ManaRegenSpeedRatio;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Updated From Actor", meta = (ToolTip = "Is the character silenced. Use Update each frame to update the component from parent actor."))
		bool IsSilenced;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Updated From Actor", meta = (ToolTip = "Updated from actor on Update, it tells if we are in control"))
		bool IsNotInControl;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Updated From Actor", meta = (ToolTip = "Updated from actor, the current movement speed of the actor."))
		float MovementSpeed;


	//Delegates/Event dispatchers
	UPROPERTY(BlueprintAssignable, Category = Default)
		FInterruptedCast OnInterruptedCast;

	//Delegates/Event dispatchers
	UPROPERTY(BlueprintAssignable, Category = Default)
		FSuccessfulCast OnSuccessfulCast;

	//Delegates/Event dispatchers
	UPROPERTY(BlueprintAssignable, Category = Default)
		FStartedCast OnStartedCast;

	//Delegates/Event dispatchers
	UPROPERTY(BlueprintAssignable, Category = Default)
		FFailedStartCast OnFailedStartCast;

	//Delegates/Event dispatchers
	UPROPERTY(BlueprintAssignable, Category = Default)
		FIntermediateEventTriggered OnIntermediateEventTriggered;


	//Functions
	UFUNCTION(BlueprintCallable, Category = Casting, meta = (ToolTip = "Interrupts the current cast, if any"))
	void InterruptCast(float interuptDuration);

	UFUNCTION(BlueprintCallable, Category = Casting, meta = (ToolTip = "Interrupts the current cast, if any"))
		bool StartCast(FCastingData castingData);

	UFUNCTION(BlueprintCallable, Category = Casting, meta = (ToolTip = "Checks if the spell cast is possible at the moment."))	
		ECastFailReason TryStartCast(FCastingData castingData);

	UFUNCTION(BlueprintCallable, Category = Internal, meta = (ToolTip = ""))
	void ApplyCastingSpeedRatio();

	UFUNCTION(BlueprintCallable, Category = Internal, meta = (ToolTip = "Called when the cast is succesful."))
	void SuccessfulCast();

	UFUNCTION(BlueprintCallable, Category = Casting, meta = (ToolTip = "Trigers a global CD. Use this for instant casts if not used through the casting system."))
	void TriggerGlobalCD(float currCastTime);

	UFUNCTION(BlueprintCallable, Category = Casting, meta = (ToolTip = "Applies the resource change. This is automatically done after successful cast but you might also want to do it for interrupted casts etc."))
	void ApplyResourceChange();

	UFUNCTION(BlueprintCallable, Category = Casting, meta = (ToolTip = "Reverts the resource change from ApplyResourceChange. This is useful if for example the spell is cast but your checks after determine that the target is not in line of sight and it cannot hit, thus the resources should be refunded."))
		void RevertResourceChange();

	UFUNCTION(BlueprintCallable, Category = Casting, meta = (ToolTip = "Needs to be called every tick to update the component"))
		void Update(float NewCastingSpeed, bool bIsNotInControl, float fMovementSpeed, float DeltaTime);

	UFUNCTION(BlueprintCallable, Category = Default, meta = (ToolTip = ""))
		void UpdateGlobalCD(float DeltaTime);


};

