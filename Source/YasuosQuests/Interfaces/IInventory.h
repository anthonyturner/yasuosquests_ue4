// Copyright 2017 @Anthony Turner

#pragma once
#include "Interactables/BaseItem.h"
#include "IInventory.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UIInventory : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class YASUOSQUESTS_API IIInventory
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	virtual void UseItem(FItemInfoStruct itemInfo) = 0;
	virtual void UnUseItem(FItemInfoStruct itemInfo) = 0;

	virtual void DropItem(FItemInfoStruct itemInfo) = 0;
	virtual void EnableActionBar(FItemInfoStruct itemInfo) = 0;
	
};
