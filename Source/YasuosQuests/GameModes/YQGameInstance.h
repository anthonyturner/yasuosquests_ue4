// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "Data/GameDataTables.h"
#include "YQGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API UYQGameInstance : public UGameInstance
{
	GENERATED_BODY()
		UYQGameInstance();  



public:  


//Functions
		void Init();
		FWeaponInfoStruct* FetchWeapon(FName);
		FSpellsData* FetchSpell(FName name);
		int GetWeaponTableSize();

//Properties - member variabls
		UPROPERTY()
		AGameDataTables* dataTables;

protected:
	bool isInitialized;

		
};
