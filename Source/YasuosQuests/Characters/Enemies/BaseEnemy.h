// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Interfaces/IInteractable.h"

#include "Interactables/BaseInteractables.h"


#include "Controllers/EnemyAIController.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"
#include "Waypoints/EnemyTargetPoint.h"
#include "Data/FEnemyInfo.h"

#include "Components/LootComponent.h"

#include "BaseEnemy.generated.h"


UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EEnemyStateType : uint8
{
	None 	UMETA(DisplayName = "None"),
	Patrol 	UMETA(DisplayName = "Patrol"),
	Chase 	UMETA(DisplayName = "Chase"),
	Roam	UMETA(DisplayName = "Roam"),
	Attack	UMETA(DisplayName = "Attack"),
	RunAway	UMETA(DisplayName = "RunAway")


};

UCLASS()
class YASUOSQUESTS_API ABaseEnemy : public ACharacter, public IIInteractable
{
	GENERATED_BODY()

public:

	ABaseEnemy();
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void PostInitializeComponents() override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void OnEnemyHit(float usedAmount);


	virtual void RegisterInteraction(IIInteractable* interact) override;
	virtual void UnRegisterInteraction(IIInteractable* interact) override;
	//classes using this interface must implement Interact
	virtual void Interact(APawn* instigator) override;

	virtual void PlaySpottedSound();
	virtual void PlayPlayerLostSound();

	void Walk();
	void Run();
	bool HasReachedDestination(FVector location);
	float GetChaseDistance();
	
	
	bool IsInChaseDistance(APawn* target);
	
	APawn* GetTarget();
	void SetTarget(APawn* pawn);

	float GetSightRadius();

	UFUNCTION()
		void RangeSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		float walkSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		float runSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		float RecentDamageTaken;

	UFUNCTION()
		void FaceRotation(FRotator NewRotation, float DeltaTime);
	
	UFUNCTION()
		virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	float CustomTakeDamage(float Damage, class AActor* DamageCauser, EDamageType damageType);
	//Hear the player's noise using the sensing component 
	UFUNCTION()
		void OnHearNoise(APawn *OtherActor, const FVector& Location, float Volume);



	UFUNCTION()////UPrimitiveComponent*, OverlappedComponent, AActor*, OtherActor, UPrimitiveComponent*, OtherComp, int32, OtherBodyIndex, bool, bFromSweep, const FHitResult &, SweepResult);
		void OnPlayerSeen(APawn* pawn);

	UFUNCTION()
	void OnPlayerNotSeen(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


	//------------------------------------------------------------------------------------------------
														//Attacking 

	bool IsHit();
	void SetHit(bool);

	void SetAttacking(bool);
	
	void PerformAttack(FVector attackLocation);

	UFUNCTION(BlueprintCallable, Category = EnemyProperties)
		bool IsAttacking();

	float GetAttackRange();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyAnims)
		TArray<UAnimMontage*>attackAnims;

	int comboCounter;

	bool isSavingAttack;

	UFUNCTION(BlueprintCallable, Category = EnemyProperties)
		void ResetCombo();

	UFUNCTION(BlueprintCallable, Category = EnemyProperties)
		void SaveComboAttack();
	
	float GetAttackDamage();

	bool IsInAttackDistance(APawn* target);
		
	UFUNCTION(BlueprintCallable, Category = "Enemy AI")
		void Attack();

	UFUNCTION(BlueprintCallable, Category = "Enemy Actions")
		void OnPostHit();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties, meta = (AllowPrivateAccess = "true"))
		bool isHit;
	
	UPROPERTY()
	bool bIsAttacking;

	// The ABaseWeapon class the enemy uses
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponInfo)
		TSubclassOf<ABaseWeapon> WeaponClass;

	UPROPERTY()
		ABaseWeapon* weapon;

	void AddLootItem(FItemInfoStruct item);
	//------------------------------------------------------------------------------------------------


	UFUNCTION(BlueprintCallable, Category = "Enemy AI")
		bool IsAlive();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		TArray<AActor*>waypoints;

	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		UAudioComponent* enemyAudioComponent;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		ULootComponent* lootTableComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		FEnemyInfoStruct enemyInfo;


	
	UFUNCTION(BlueprintCallable, Category = EnemyProperties)
	int GetCurrentHealth();

	void ChangeHealth(int amount);

	void SetCurrentHealth(int amount);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = EnemyProperties, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* rangeSphereCollision;

protected:

	UPROPERTY(VisibleAnyWhere, Category = EnemyProperties, meta = (AllowPrivateAccess = "true"))
		class UPawnSensingComponent* PawnSensingComp;

	
private:

	UPROPERTY()
	float timer;

	
	FNavLocation navLocation;
		
	
	UPROPERTY()
	FTimerHandle dieTimerHandle;

	UPROPERTY()
	APawn* target;

	UPROPERTY()
	bool playerSeen;

	//Check if the enemy is dead or alive 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties, meta = (AllowPrivateAccess = "true"))
		bool IsDead;


	//-----------------------------------------------------------------------------------------------------------------
	//											Private functions
	//-----------------------------------------------------------------------------------------------------------------


	void RemoveEnemy();
	void Die();
	void DropItems();

	UPROPERTY()
	TArray<ABaseItem*>lootItems;
	float dropChance;
};
