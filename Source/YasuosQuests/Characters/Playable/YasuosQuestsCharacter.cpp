// Fill out your copyright notice in the Description page of Project Settings.
#define LOCTEXT_NAMESPACE "YQCharacterNamespace" 

#include "YasuosQuests.h"
#include "YasuosQuestsCharacter.h"
#include "SceneCaptures/CharacterRecorder.h"
#include "Animation/AnimInstance.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AYasuosQuestsCharacter::AYasuosQuestsCharacter()
{
	//Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	jumpingVelocity = 600.f;
	//set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	//Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	//Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = jumpingVelocity;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->MaxWalkSpeed = 675.f;

	//Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller


												//Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);// Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	SK_HeadMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HeadsUSkeletalMeshComponent"));
	SK_HeadMesh->SetupAttachment(GetMesh());//AttachTo
	SK_TorsoMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TorsoUSkeletalMeshComponent"));
	SK_TorsoMesh->SetupAttachment(GetMesh());//AttachTo
	SK_ArmsMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ArmsUSkeletalMeshComponent"));
	SK_ArmsMesh->SetupAttachment(GetMesh());//AttachTo
	SK_LegsMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("LegsUSkeletalMeshComponent"));
	SK_LegsMesh->SetupAttachment(GetMesh());//AttachTo
	SK_FeetMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FeetUSkeletalMeshComponent"));
	SK_FeetMesh->SetupAttachment(GetMesh());//AttachTo

	CastingSystem = CreateDefaultSubobject<UCastingSystemComponent>(TEXT("UCastingSystemComponent"));

	characterInfo.manaRegenPerSec = 4.f;
	characterInfo.maxHealth = 100.f;
	characterInfo.maxMana = 100.f;
	characterInfo.health = characterInfo.maxHealth;
	characterInfo.mana = characterInfo.maxMana;
	characterInfo.playerLevel = 1;
	characterInfo.experience = 0.2;
	characterInfo.attackDamage = 5;
	characterInfo.defense = 10;
	characterInfo.skillPoints = 0;

	isAlive = true;
	maxLADToInteractable = .8f;
	maxInventorySize = 25;
	isWeaponSheathed = false;
	bIsAttacking = false;
	comboCounter = 0;
	attackingSpeed = 300.f;
	//by default the inputs should be enabled, in case there is something ned to be tested
	OnSetPlayerController(true);

	bReplicates = true;

	//	static ConstructorHelpers::FObjectFinder<UBlueprint> SpellItemBlueprint(TEXT("Blueprint'/Game/SpellAndDamageSystem/Examples/Spells/BP_CosmicShards.BP_CosmicShards'"));

}


//PostInitializeComponents - Called after the Actor's components have been initialized
void AYasuosQuestsCharacter::PostInitializeComponents() {

	Super::PostInitializeComponents();



}



//////////////////////////////////////////////////////////////////////////
// Input

void AYasuosQuestsCharacter::SetupPlayerInputComponent(class UInputComponent* IComponent)
{

	Super::SetupPlayerInputComponent(IComponent);

	//Set up gameplay key bindings
	check(IComponent);
	InputComponent->BindAction("Jump", IE_Pressed, this, &AYasuosQuestsCharacter::OnStartJump);
	InputComponent->BindAction("Jump", IE_Released, this, &AYasuosQuestsCharacter::OnStopJumping);

	InputComponent->BindAction("Run", IE_Pressed, this, &AYasuosQuestsCharacter::Run);
	InputComponent->BindAction("Run", IE_Released, this, &AYasuosQuestsCharacter::Walk);

	InputComponent->BindAction("Interact", IE_Pressed, this, &AYasuosQuestsCharacter::PerformInteract);
	InputComponent->BindAction("Pickup", IE_Pressed, this, &AYasuosQuestsCharacter::PerformPickup);
	InputComponent->BindAction("Inventory", IE_Pressed, this, &AYasuosQuestsCharacter::ShowInventory);
	InputComponent->BindAction("Equipment", IE_Pressed, this, &AYasuosQuestsCharacter::ShowEquipment);
	InputComponent->BindAction("ResetCamera", IE_Pressed, this, &AYasuosQuestsCharacter::ResetCamera);


	InputComponent->BindAction("Attack", IE_Pressed, this, &AYasuosQuestsCharacter::PerformAttack);
	InputComponent->BindAction("ToggleWeapon", IE_Pressed, this, &AYasuosQuestsCharacter::ToggleWeapon);
	InputComponent->BindAction("SpellOneAbility", IE_Pressed, this, &AYasuosQuestsCharacter::SpellOneAbility);
	InputComponent->BindAction("SpellTwoAbility", IE_Pressed, this, &AYasuosQuestsCharacter::SpellTwoAbility);
	InputComponent->BindAction("SpellThreeAbility", IE_Pressed, this, &AYasuosQuestsCharacter::SpellThreeAbility);
	InputComponent->BindAction("SpellFourAbility", IE_Pressed, this, &AYasuosQuestsCharacter::SpellFourAbility);

	InputComponent->BindAxis("MoveForward", this, &AYasuosQuestsCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AYasuosQuestsCharacter::MoveRight);

	//We have 2 versions of the rotation bindings to handle different kinds of devices differently
	//"turn" handles devices that provide an absolute delta, such as a mouse.
	//"turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &AYasuosQuestsCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &AYasuosQuestsCharacter::LookUpAtRate);




}

//Called when the level is started
void AYasuosQuestsCharacter::BeginPlay() {

	Super::BeginPlay();

	gameInstance = Cast<UYQGameInstance>(GetGameInstance());
	if (gameInstance) {

		FSpellsData spellInfo = *gameInstance->FetchSpell("CosmicShards");
		spellOne = spellInfo;

		spellInfo = *gameInstance->FetchSpell("InstantHeal");
		spellTwo = spellInfo;

		spellInfo = *gameInstance->FetchSpell("GroundWave");
		spellThree = spellInfo;

		spellInfo = *gameInstance->FetchSpell("SolarFlare");
		spellFour = spellInfo;


	}

	UWorld* world = GetWorld();

	if (world) {
		if (gameInstance) {
			FString IndexString = FString::FromInt((int32)1);
			FName IndexName = FName(*IndexString);

			FWeaponInfoStruct* weaponInfo = gameInstance->FetchWeapon(IndexName);
			if (weaponInfo) {

				FItemInfoStruct itemInfo;
				itemInfo.CreateItem(*weaponInfo);

				ABaseWeapon* weapon = world->SpawnActor<ABaseWeapon>(weaponInfo->weaponClass, GetActorLocation(), GetActorRotation());
				weapon->weaponInfo = *weaponInfo;
				weapon->itemInfo = itemInfo;
				weapon->interactableMesh->SetSkeletalMesh(itemInfo.itemSKMesh);

			}

		}

		if (hudWidgetClass) {
			hudWidget = CreateWidget<UHudWidget>(world, hudWidgetClass);
			hudWidget->AddToViewport();
		}

		if (EquipmentWidgetClass) {
			equipmentWidget = CreateWidget<UEquipmentWidget>(world, EquipmentWidgetClass);
		}

		if (InventoryWidgetClass)
			inventoryWidget = CreateWidget<UInventoryWidget>(world, InventoryWidgetClass);

	}
	Walk();




	InitCastingSystem();
}


// Called every frame
void AYasuosQuestsCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdateBaseInteractable();
	AdjustCamera();
	UpdateStats(DeltaTime);
	UpdateUserInterfaceBar("Mana", CastingSystem->Resource1 / CastingSystem->MaxResource1);

	//UCharacterMovementComponent* cm = GetCharacterMovement();
	//if (cm) {

	//castingSysComp->Update(CastingSpeed, !IsInControl, cm->Velocity.Size(), DeltaTime);

	//	}


}

//Bind all casting events so we an react to them
void AYasuosQuestsCharacter::InitCastingSystem() {


	IsInControl = true;
	CastingSystem->OnSuccessfulCast.AddDynamic(this, &AYasuosQuestsCharacter::OnSpellCasted);
	CastingSystem->OnStartedCast.AddDynamic(this, &AYasuosQuestsCharacter::OnCastStarted);
	CastingSystem->OnFailedStartCast.AddDynamic(this, &AYasuosQuestsCharacter::OnFailedStartCast);
	CastingSystem->OnInterruptedCast.AddDynamic(this, &AYasuosQuestsCharacter::OnInterrupted);
	CastingSystem->OnIntermediateEventTriggered.AddDynamic(this, &AYasuosQuestsCharacter::OnIntermediateEventTriggered);

}


void AYasuosQuestsCharacter::SpellOneAbility() {


	FCastingData castingData;

	castingData.SpellName = spellOne.SpellName;
	castingData.CastTime = spellOne.CastTime;
	castingData.CanMoveWhileCasting = spellOne.CanMoveWhileCasting;
	castingData.Resource1Use = spellOne.ManaCost;
	castingData.Resource2Use = 0.f;
	castingData.DoesNoTriggerGlobalCD = spellOne.DoesNoTriggerGlobalCD;
	castingData.CanBeUsedNotInControl = spellOne.CanBeUsedNotInControl;
	CastingSystem->StartCast(castingData);

}

void AYasuosQuestsCharacter::SpellTwoAbility() {




	FCastingData castingData;// = spellsList[0];

	castingData.SpellName = spellTwo.SpellName;
	castingData.CastTime = spellTwo.CastTime;
	castingData.CanMoveWhileCasting = spellTwo.CanMoveWhileCasting;
	castingData.Resource1Use = spellTwo.ManaCost;
	castingData.Resource2Use = 0.f;
	castingData.DoesNoTriggerGlobalCD = spellTwo.DoesNoTriggerGlobalCD;
	castingData.CanBeUsedNotInControl = spellTwo.CanBeUsedNotInControl;

	CastingSystem->StartCast(castingData);
}

void AYasuosQuestsCharacter::SpellThreeAbility() {

	FCastingData castingData;// = spellsList[0];

	castingData.SpellName = spellThree.SpellName;
	castingData.CastTime = spellThree.CastTime;
	castingData.CanMoveWhileCasting = spellThree.CanMoveWhileCasting;
	castingData.Resource1Use = spellThree.ManaCost;
	castingData.Resource2Use = 0.f;
	castingData.DoesNoTriggerGlobalCD = spellThree.DoesNoTriggerGlobalCD;
	castingData.CanBeUsedNotInControl = spellThree.CanBeUsedNotInControl;

	CastingSystem->StartCast(castingData);
}

void AYasuosQuestsCharacter::SpellFourAbility() {

	FCastingData castingData;// = spellsList[0];

	castingData.SpellName = spellFour.SpellName;
	castingData.CastTime = spellFour.CastTime;
	castingData.CanMoveWhileCasting = spellFour.CanMoveWhileCasting;
	castingData.Resource1Use = spellFour.ManaCost;
	castingData.Resource2Use = 0.f;
	castingData.DoesNoTriggerGlobalCD = spellFour.DoesNoTriggerGlobalCD;
	castingData.CanBeUsedNotInControl = spellFour.CanBeUsedNotInControl;

	CastingSystem->StartCast(castingData);
}

FCastingData AYasuosQuestsCharacter::GetCastingData(FName name) {

	FSpellsData* spellInfo = gameInstance->FetchSpell(name);
	FCastingData castingData;

	if (spellInfo) {

		castingData.SpellName = spellInfo->SpellName;
		castingData.CastTime = spellInfo->CastTime;
		castingData.CanMoveWhileCasting = spellInfo->CanMoveWhileCasting;
		castingData.Resource1Use = spellInfo->ManaCost;
		castingData.Resource2Use = 0.f;
		castingData.DoesNoTriggerGlobalCD = spellInfo->DoesNoTriggerGlobalCD;
		castingData.CanBeUsedNotInControl = spellInfo->CanBeUsedNotInControl;
	}
	else {

		castingData.SpellName = "";
	}

	return castingData;
}

//Casting


void AYasuosQuestsCharacter::OnCastStarted(FName SpellName) {

	UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
	if (gameInstance) {
		FSpellsData* spellInfo = gameInstance->FetchSpell(SpellName);
		if (spellInfo && spellInfo->SpellAnimation) {
			animInstance->Montage_Play(spellInfo->SpellAnimation);
		}
	}
}

void AYasuosQuestsCharacter::OnSpellCasted(FName SpellName) {
	//Change this to an array of blueprints
	//Then spawn the blueprint at the array index with the SpellName

	FSpellsData* spellInfo = gameInstance->FetchSpell(SpellName);
	if (spellInfo) {

		FVector socket_location;
		switch (spellInfo->SpellType) {

			case ESpellDamageType::Healing:

				socket_location = GetMesh()->GetSocketLocation(FName("PelvisSocket"));
				break;

			default: //Launch spell from primary attack hand if not healing type

				socket_location = GetMesh()->GetSocketLocation(FName("PrimaryRightHand"));

		}

		FVector SpawnLocation = socket_location + socket_location.ForwardVector * spellInfo->SpellStartOffset;
		FRotator SpawnRotation = GetActorRotation();
		//FTransform transform(SpawnRotation, (SpawnRotation.Vector().ForwardVector * spellInfo->SpellStartOffset) + SpawnLocation);
		FTransform transform(SpawnRotation, SpawnLocation);
		FTransform actorTransfrom(GetActorRotation(), SpawnLocation);
		UWorld* world = GetWorld();
		if (world) {

			AActor* actor = world->SpawnActor<AActor>(spellInfo->BP_Spell, transform);
			if (actor)
				OnSpellInitialize(actor);
		}
	}
	else {

		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "No spell info in OnSpellCasted " + SpellName.ToString());

	}
}


void AYasuosQuestsCharacter::OnIntermediateEventTriggered(FName SpellName) {

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, "OnIntermediateEventTriggered ");

	if (SpellName.Compare(FName("InstantHeal")) == 0) {

		//HealSpell();
	}

}



void AYasuosQuestsCharacter::OnInterrupted(FName SpellName, float InteruptDuration) {

	//ECastFailReason::Interrupted change to ENUM
	CastingError = LOCTEXT("CastFailReason", "Interrupted");
	//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, "OnInterrupted");


}

void AYasuosQuestsCharacter::OnFailedStartCast(FName SpellName, ECastFailReason reason) {

	switch (reason) {

	case ECastFailReason::NoReason:
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "NoReason");
		CastingError = LOCTEXT("CastFailReason", "");
		break;

	case ECastFailReason::NotEnoughResource1:
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "NotEnoughResource1");
		CastingError = LOCTEXT("CastFailReason", "Not enough resource 1");
		break;

	case ECastFailReason::NotEnoughResource2:
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "NotEnoughResource2");
		CastingError = LOCTEXT("CastFailReason", "Not enough resource 2");
		break;

	case ECastFailReason::Interrupted:
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Interrupted");
		CastingError = LOCTEXT("CastFailReason", "Interrupted");
		break;

	case ECastFailReason::Silenced:
		CastingError = LOCTEXT("CastFailReason", "Silenced");
		break;

	case ECastFailReason::NotInControl:
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "NotInControl");
		CastingError = LOCTEXT("CastFailReason", "Not in control");
		break;

	case ECastFailReason::GlobalCooldownNotAvailable:
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "GlobalCooldownNotAvailable");
		CastingError = LOCTEXT("CastFailReason", "Global CD not available");
		break;

	case ECastFailReason::AlreadyCasting:
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "AlreadyCasting");
		CastingError = LOCTEXT("CastFailReason", "Already casting");
		break;
	}

}




void AYasuosQuestsCharacter::OnSpellInitialize_Implementation(AActor* actor) {

}

//------------------------------------------------------------------------Interactions------------------------------------------------------------------------------
void AYasuosQuestsCharacter::UpdateStats(float delta) {


	float regenMana = characterInfo.manaRegenPerSec * delta;
	AddMana(regenMana);

}

void AYasuosQuestsCharacter::UpdateBaseInteractable() {

	GetBestInteractable();
}

FInteractablesDataStruct AYasuosQuestsCharacter::GetBestInteractable() {

	FInteractablesDataStruct bestInterActable;
	//Iterate through all interactables the player interacts with and highlight the one the player is closests to looking at.
	for (ABaseInteractables* interactable : interactables)
	{
		FVector playerLocation = GetActorLocation();
		FVector interactableMass = interactable->interactableMesh->GetComponentLocation();
		FVector v = (interactableMass - playerLocation).GetSafeNormal();
		FVector distanceNorm(FMath::Abs(v.X), FMath::Abs(v.Y), FMath::Abs(v.Z));
		float lookAngleDot = FVector::DotProduct(distanceNorm, playerLocation.ForwardVector);//Dot product of the player looking at the object ( a value of 1 ) or the player looking away ( a value of -1)

		if (lookAngleDot > maxLADToInteractable && lookAngleDot > bestInterActable.dotProduct) {//Max look away distance from interactable

			bestInterActable.dotProduct = lookAngleDot;
			bestInterActable.interactable = interactable;
		}
	}

	SetBestInteractable(bestInterActable.interactable);

	return bestInterActable;

}


void AYasuosQuestsCharacter::SetBestInteractable(ABaseInteractables* interactable) {

	if (interactable) {

		if (highLightedInteractable != interactable) {

			if (highLightedInteractable == nullptr) {

				highLightedInteractable = interactable;
				highLightedInteractable->SetHighlight(true);
			}
			else {
				highLightedInteractable->SetHighlight(false);
				highLightedInteractable = interactable;
			}
		}

	}
	else {

		if (highLightedInteractable != nullptr) {

			highLightedInteractable->SetHighlight(false);
			highLightedInteractable = nullptr;
		}
	}
}

void AYasuosQuestsCharacter::HandleRegisterInteraction(ABaseInteractables* interactable) {


	APickupItem* pickupItem = Cast<APickupItem>(interactable);
	if (pickupItem) {

		pickupItem->ShowItemInfo();
	}
	interactables.AddUnique(interactable);
}

void AYasuosQuestsCharacter::HandleUnRegisterInteraction(ABaseInteractables* interactable) {

	APickupItem* pickupItem = Cast<APickupItem>(interactable);
	if (pickupItem) {

		pickupItem->HideItemInfo();
	}
	interactables.Remove(interactable);
}


void AYasuosQuestsCharacter::RegisterInteraction(IIInteractable* interact) {

	ABaseInteractables* interactable = Cast<ABaseInteractables>(interact);
	if (interactable) {

		HandleRegisterInteraction(interactable);
	}
}

void AYasuosQuestsCharacter::UnRegisterInteraction(IIInteractable* interact) {

	ABaseInteractables* interactable = Cast<ABaseInteractables>(interact);
	if (interactable) {
		HandleUnRegisterInteraction(interactable);
	}
}

void AYasuosQuestsCharacter::PerformInteract() {

	Interact(this);
}

void AYasuosQuestsCharacter::PerformPickup() {

	if (highLightedInteractable) {

		if (HasSpaceInInventory()) {//Inventory is less than max allowed inventory size

			APickupItem* pickupItem = Cast<APickupItem>(highLightedInteractable);
			//pickupItem->DestroyWorldComponents();
			if (pickupItem) {
				if (pickupItem->itemInfo.itemPickupSound) {
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), pickupItem->itemInfo.itemPickupSound, pickupItem->GetActorLocation());
				}
				pickupItem->Pickup(this);
			}
		}
	}
}

void AYasuosQuestsCharacter::UseItem(FItemInfoStruct itemInfo) {

	RemoveFromInventory(itemInfo);
	switch (itemInfo.itemType) {

	case EItemType::Armor: //Equip Armor
		UpdateEquipmentSlot(itemInfo);//Update equipment UI
		AttachEquipment(itemInfo);
		if (recorderReference)
			recorderReference->EquipItem(itemInfo);
		break;
	case EItemType::Weapon://Equip/attach weapon
		UpdateEquipmentSlot(itemInfo);//Update equipment UI
		AttachEquipment(itemInfo);
		if (recorderReference)
			recorderReference->EquipItem(itemInfo);
		if (!HasWeaponSheathed()) {//Only show attack stance if weapon is out and not sheathed ( not put away )
			isWeaponAttached = true;
			bUseControllerRotationYaw = isWeaponAttached;
		}
		else {

			isWeaponAttached = false;
			bUseControllerRotationYaw = isWeaponAttached;
		}
		break;
	}

	if (itemInfo.itemEquipSound)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), itemInfo.itemEquipSound, GetActorLocation());
}

void  AYasuosQuestsCharacter::UnUseItem(FItemInfoStruct itemInfo) {

	switch (itemInfo.itemType) {

	case EItemType::Armor:
		RemoveFromEquipment(itemInfo);
		AddToInventory(itemInfo);
		RefreshEquipment();
		UnAttachEquipment(itemInfo);
		if (recorderReference)
			recorderReference->UnEquipItem(itemInfo);
		break;
	case EItemType::Weapon:

		RemoveFromEquipment(itemInfo);
		AddToInventory(itemInfo);
		RefreshEquipment();
		UnAttachEquipment(itemInfo);
		if (recorderReference)
			recorderReference->UnEquipItem(itemInfo);
		isWeaponAttached = false;
		bUseControllerRotationYaw = isWeaponAttached;
		isWeaponSheathed = false;
		Walk();
		break;
	}

	if (itemInfo.itemUnEquipSound)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), itemInfo.itemUnEquipSound, GetActorLocation());

}

void  AYasuosQuestsCharacter::DropItem(FItemInfoStruct itemInfo) {

	RemoveFromInventory(itemInfo);
	RefreshInventory();
	SpawnDroppedItem(itemInfo);
	if (itemInfo.itemDropSound)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), itemInfo.itemDropSound, GetActorLocation());
}

void  AYasuosQuestsCharacter::EnableActionBar(FItemInfoStruct itemInfo) {


}

void AYasuosQuestsCharacter::SpawnDroppedItem(FItemInfoStruct itemInfo) {

	FVector location = GetArrowComponent()->GetComponentLocation();
	FVector forwardVector = GetArrowComponent()->GetForwardVector() * 200.f;
	const FTransform loc(location + forwardVector);


	ABaseItem* dropped = GetWorld()->SpawnActor<ABaseItem>(itemInfo.itemClass, loc);
	dropped->itemInfo = itemInfo;

	FName weaponName;
	FWeaponInfoStruct* weaponInfo = nullptr;
	switch (itemInfo.itemType) {//Since the extended item info is not stored, fetch it from the database

	case EItemType::Armor: //Equip Armor

		break;
	case EItemType::Weapon://Equip/attach weapon

		weaponName = itemInfo.item_id;
		weaponInfo = gameInstance->FetchWeapon(weaponName);
		(Cast<ABaseWeapon>(dropped))->weaponInfo = *weaponInfo;

		break;
	}


	dropped->interactableMesh->SetSkeletalMesh(itemInfo.itemSKMesh);
	dropped->interactableMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	dropped->interactableMesh->SetSimulatePhysics(true);
}

//------------------------------------------------------------------------Inventory------------------------------------------------------------------------------
UInventoryWidget* AYasuosQuestsCharacter::GetInventoryWidget() {

	return inventoryWidget;
}

TArray<FItemInfoStruct> AYasuosQuestsCharacter::GetInventory() {

	return inventory;
}




int AYasuosQuestsCharacter::GetMaxInventorySize() {

	return maxInventorySize;
}


void AYasuosQuestsCharacter::ShowInventory() {

	APlayerController* pc = Cast<APlayerController>(Controller);
	if (pc) {

		if (isInventoryOpen) {

			inventoryWidget->RemoveFromViewport();

			if (isEquipmentOpen) {

				isInventoryOpen = false;
			}
			else {

				FInputModeGameOnly mode;
				pc->SetInputMode(mode);
				pc->bShowMouseCursor = false;
				isInventoryOpen = false;
			}
			if (inventoryCloseSound)
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), inventoryCloseSound, GetActorLocation());
		}
		else {

			inventoryWidget->AddToViewport(1);

			FInputModeGameAndUI mode;
			mode.SetWidgetToFocus(inventoryWidget->GetCachedWidget());
			mode.SetHideCursorDuringCapture(true);

			pc->SetInputMode(mode);
			pc->bShowMouseCursor = true;
			isInventoryOpen = true;
			if (inventoryOpenSound)
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), inventoryOpenSound, GetActorLocation());
		}
	}


}

void AYasuosQuestsCharacter::AddToInventory(FItemInfoStruct item) {

	if (inventoryWidget) {

		inventory.Add(item);
		RefreshInventory();
	}
}

void AYasuosQuestsCharacter::RemoveFromInventory(FItemInfoStruct item) {
	if (inventory.IsValidIndex(item.itemIndex)) {
		inventory.RemoveAt(item.itemIndex);
	}
}


void AYasuosQuestsCharacter::RefreshInventory() {

	if (inventoryWidget) {

		inventoryWidget->RefreshInventory_Implementation();
	}
}

bool AYasuosQuestsCharacter::HasSpaceInInventory() {

	return (inventory.Num() < maxInventorySize);
}

//------------------------------------------------------------------------Equipment------------------------------------------------------------------------------
void AYasuosQuestsCharacter::ShowEquipment() {

	APlayerController* pc = Cast<APlayerController>(Controller);
	if (pc) {
		if (isEquipmentOpen) {

			isEquipmentOpen = false;
			equipmentWidget->RemoveFromViewport();

			if (isInventoryOpen) {

				isEquipmentOpen = false;
			}
			else {

				FInputModeGameOnly mode;
				pc->SetInputMode(mode);
				pc->bShowMouseCursor = false;
				isEquipmentOpen = false;
			}

		}
		else {

			if (EquipmentWidgetClass) {

				equipmentWidget = CreateWidget<UEquipmentWidget>(Controller->GetWorld(), EquipmentWidgetClass);
				equipmentWidget->AddToViewport(1);
				isEquipmentOpen = true;
				FInputModeGameAndUI mode;
				mode.SetWidgetToFocus(equipmentWidget->GetCachedWidget());
				mode.SetHideCursorDuringCapture(true);
				pc->SetInputMode(mode);
				pc->bShowMouseCursor = true;

			}
		}
	}
}



TArray<FItemInfoStruct> AYasuosQuestsCharacter::GetEquipmentInventory() {

	return equipmentInventory;
}

void AYasuosQuestsCharacter::AddToEquipment(FItemInfoStruct item) {

	equipmentInventory.Add(item);

	if (equipmentWidget) {
		equipmentWidget->CreateEquipment();
	}

}


void AYasuosQuestsCharacter::RemoveFromEquipment(FItemInfoStruct item) {

	if (equipmentInventory.IsValidIndex(item.itemIndex)) {
		equipmentInventory.RemoveAt(item.itemIndex);
	}

}

void AYasuosQuestsCharacter::RefreshEquipment() {

	equipmentWidget->CreateEquipment();

}

void AYasuosQuestsCharacter::UpdateEquipmentSlot(FItemInfoStruct item) {

	if (equipmentWidget)
		equipmentWidget->OnUpdateEquipmentSlot_Implementation(item);
	else {
		AddToEquipment(item);
		RefreshInventory();
	}


}

void AYasuosQuestsCharacter::SpawnAttachedEquipment(ABaseItem* item, FItemInfoStruct itemInfo) {

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	//const FTransform* transform = GetActorTransform();

	switch (itemInfo.itemType) {

	case EItemType::Armor:
		//Attach armor to skeletal mesh component
		item = GetWorld()->SpawnActor<ABaseArmor>(itemInfo.itemClass, GetActorTransform(), spawnParams);

		if (itemInfo.itemSlot == ESlotType::Torso) {

			SK_TorsoMesh->SetSkeletalMesh(item->interactableMesh->SkeletalMesh);
			item->Destroy();
		}
		else {
			//	item->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, item->armorInfo.AttachPoint);
			item->SetActorEnableCollision(false);
		}
		break;

	case EItemType::Weapon:
		//Spawn and attach weapons and shields to sockets and not the skeletal mesh
		ABaseWeapon* weapon = GetWorld()->SpawnActor<ABaseWeapon>(itemInfo.itemClass, GetActorTransform(), spawnParams);
		item = weapon;
		if (item) {

			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, "Item spawned  " + item->GetName());
			if (PrimaryRightHandItem) {
				GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, "PrimaryRightHandItem spawned  " + PrimaryRightHandItem->GetName());
			}
			PrimaryRightHandItem = item;
		}
		FName slot;
		switch (itemInfo.itemSlot) {

		case  ESlotType::PrimaryLeftHand:
			//	slot = item->weaponInfo.PrimaryLeftHand;
			break;

		case  ESlotType::PrimaryRightHand:
			slot = "PrimaryRightHand";//item->weaponInfo.PrimaryRightHand;
			break;

		case  ESlotType::SecondaryLeftHand:
			//	slot = item->weaponInfo.SecondaryLeftHand;
			break;
		case  ESlotType::SecondaryRightHand:
			//slot = item->weaponInfo.SecondaryRightHand;
			break;
		}
		item->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, slot);
		item->SetActorEnableCollision(false);
		attackAnims.Insert(weapon->weaponInfo.AttackMontage, 0);//Temp assign the weapons animation to the current player's animation - combo_1
		break;
	}
}


void AYasuosQuestsCharacter::AttachEquipment(FItemInfoStruct itemInfo) {

	if (!HasWeaponSheathed()) {

		FActorSpawnParameters spawnParams;
		spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ABaseWeapon* weapon = nullptr;
		ABaseArmor* armor = nullptr;
		FName weaponName;
		FWeaponInfoStruct* weaponInfo = nullptr;

		//Any attached equipment is a pickup item
		//Spawn it as pickup to destroy its pickup point light and rotation component
		//Then, based on the item type, cast it to that type when we finish spawning.
		APickupItem* pickupItem = GetWorld()->SpawnActorDeferred<APickupItem>(itemInfo.itemClass, GetActorTransform(), this, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		pickupItem->DestroyWorldComponents();
		UGameplayStatics::FinishSpawningActor(pickupItem, pickupItem->GetActorTransform());

		switch (itemInfo.itemType) {

		case EItemType::Weapon:

			weapon = Cast<ABaseWeapon>(pickupItem);
			weaponInfo = gameInstance->FetchWeapon(itemInfo.item_id);
			if (weapon && weaponInfo) {
				weapon->SetOwner(this);
				weapon->weaponInfo = *weaponInfo;
				weapon->itemInfo = itemInfo;
				weapon->interactableMesh->SetSkeletalMesh(weaponInfo->weaponMesh);
				attackAnims.Insert(weapon->weaponInfo.AttackMontage, 0);//Temp assign the weapons animation to the current player's animation - combo_1
				UCharacterMovementComponent* cmc = GetCharacterMovement();
				if (cmc) cmc->MaxWalkSpeed = attackingSpeed;


				switch (itemInfo.itemSlot) {

				case  ESlotType::PrimaryLeftHand:

					if (PrimaryLeftHandItem != nullptr) {
						PrimaryLeftHandItem->Destroy();
					}
					PrimaryLeftHandItem = weapon;
					PrimaryLeftHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.PrimaryLeftHand);
					PrimaryLeftHandItem->SetActorEnableCollision(false);
					break;

				case ESlotType::PrimaryRightHand:
					if (PrimaryRightHandItem != nullptr) {
						PrimaryRightHandItem->Destroy();
					}
					PrimaryRightHandItem = weapon;
					PrimaryRightHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.PrimaryRightHand);
					PrimaryRightHandItem->SetActorEnableCollision(false);
					break;

				case  ESlotType::SecondaryLeftHand:
					if (SecondaryLeftHandItem != nullptr) {
						SecondaryLeftHandItem->Destroy();
					}
					SecondaryLeftHandItem = weapon;
					SecondaryLeftHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.SecondaryLeftHand);
					SecondaryLeftHandItem->SetActorEnableCollision(false);
					break;
				case  ESlotType::SecondaryRightHand:
					if (SecondaryRightHandItem != nullptr) {
						SecondaryRightHandItem->Destroy();
					}
					SecondaryRightHandItem = weapon;
					SecondaryRightHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.SecondaryRightHand);
					SecondaryRightHandItem->SetActorEnableCollision(false);
					break;
				}
			}
			break;

			//====================================================================================
		case EItemType::Armor:

			armor = Cast<ABaseArmor>(pickupItem);
			armor->SetOwner(this);
			armor->itemInfo = itemInfo;
			armor->SetActorEnableCollision(false);

			GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, "Character attack damage " + FString::SanitizeFloat(characterInfo.attackDamage));

			//Set character modifier stats
			characterInfo.attackDamage += armor->armorInfo.DamageRating;

			GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, "Character attack damage after armor equiped " + FString::SanitizeFloat(characterInfo.attackDamage));

			switch (itemInfo.itemSlot) {

			case ESlotType::Head:

				if (HeadItem != nullptr) {
					HeadItem->Destroy();

				}
				HeadItem = armor;
				SK_HeadMesh->SetSkeletalMesh(HeadItem->interactableMesh->SkeletalMesh);

				break;

			case ESlotType::Torso:

				if (TorsoItem != nullptr) {
					TorsoItem->Destroy();
				}
				TorsoItem = armor;
				SK_TorsoMesh->SetSkeletalMesh(TorsoItem->interactableMesh->SkeletalMesh);

				break;

			case ESlotType::Arms:
				if (RightArmItem != nullptr) {
					RightArmItem->Destroy();
				}

				if (LeftArmItem != nullptr) {
					LeftArmItem->Destroy();
				}

				RightArmItem = armor;
				armor->AttachToActor(this, FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.RightAttachPoint);

				LeftArmItem = armor;
				armor->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.LeftAttachPoint);
				armor->SetActorEnableCollision(false);

				//ArmsItem = armor;
				//SK_ArmsMesh->SetSkeletalMesh(ArmsItem->interactableMesh->SkeletalMesh);
				break;

			case ESlotType::Legs:
				if (LegsItem != nullptr) {
					LegsItem->Destroy();
				}
				LegsItem = armor;
				SK_LegsMesh->SetSkeletalMesh(LegsItem->interactableMesh->SkeletalMesh);
				break;

			case ESlotType::Feet:
				if (FeetItem != nullptr) {
					FeetItem->Destroy();
				}
				FeetItem = armor;
				SK_FeetMesh->SetSkeletalMesh(FeetItem->interactableMesh->SkeletalMesh);
				break;


			case  ESlotType::PrimaryLeftHand:

				if (PrimaryLeftHandItem != nullptr) {//Destroy previously referenced armor attached to socket
					PrimaryLeftHandItem->Destroy();
				}
				PrimaryLeftHandItem = armor;
				PrimaryLeftHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.LeftAttachPoint);
				break;

			case ESlotType::PrimaryRightHand:
				if (PrimaryRightHandItem != nullptr) {
					PrimaryRightHandItem->Destroy();
				}
				PrimaryRightHandItem = armor;
				PrimaryRightHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.RightAttachPoint);
				break;

			case  ESlotType::SecondaryLeftHand:
				if (SecondaryLeftHandItem != nullptr) {
					SecondaryLeftHandItem->Destroy();
				}
				SecondaryLeftHandItem = armor;
				SecondaryLeftHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.AttachPoint);
				break;

			case  ESlotType::SecondaryRightHand:
				if (SecondaryRightHandItem != nullptr) {
					SecondaryRightHandItem->Destroy();
				}
				SecondaryRightHandItem = armor;
				SecondaryRightHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.AttachPoint);
				break;
			}//End inner switch for Slot type of item(armor)
			armor->SetActorHiddenInGame(true);
		}//End switch for item type

	}
	else {

		SwapItemsToSecondary(itemInfo);
	}
}


//TODO! Refacor and simply code! TEMP CODE!
void AYasuosQuestsCharacter::SwapItemsToSecondary(FItemInfoStruct itemInfo) {

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	ABaseWeapon* weapon = nullptr;
	ABaseArmor* armor = nullptr;

	switch (itemInfo.itemType) {

	case EItemType::Weapon:

		weapon = GetWorld()->SpawnActor<ABaseWeapon>(itemInfo.itemClass, GetActorTransform(), spawnParams);
		weapon->itemInfo = itemInfo;
		switch (itemInfo.itemSlot) {

		case  ESlotType::PrimaryLeftHand:

			if (PrimaryLeftHandItem != nullptr) {
				PrimaryLeftHandItem->Destroy();
			}
			PrimaryLeftHandItem = weapon;
			PrimaryLeftHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.SecondaryLeftHand);
			PrimaryLeftHandItem->SetActorEnableCollision(false);
			break;

		case ESlotType::PrimaryRightHand:
			if (PrimaryRightHandItem != nullptr) {
				PrimaryRightHandItem->Destroy();
			}
			PrimaryRightHandItem = weapon;
			PrimaryRightHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.SecondaryRightHand);
			PrimaryRightHandItem->SetActorEnableCollision(false);
			break;

		case  ESlotType::SecondaryLeftHand:
			if (SecondaryLeftHandItem != nullptr) {
				SecondaryLeftHandItem->Destroy();
			}
			SecondaryLeftHandItem = weapon;
			SecondaryLeftHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.SecondaryLeftHand);
			SecondaryLeftHandItem->SetActorEnableCollision(false);
			break;
		case  ESlotType::SecondaryRightHand:
			if (SecondaryRightHandItem != nullptr) {
				SecondaryRightHandItem->Destroy();
			}
			SecondaryRightHandItem = weapon;
			SecondaryRightHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.SecondaryRightHand);
			SecondaryRightHandItem->SetActorEnableCollision(false);
			break;
		}
		break;
		//====================================================================================
	case EItemType::Armor:
		armor = GetWorld()->SpawnActor<ABaseArmor>(itemInfo.itemClass, GetActorTransform(), spawnParams);

		armor->itemInfo = itemInfo;

		switch (itemInfo.itemSlot) {

		case ESlotType::Head:

			if (HeadItem != nullptr) {
				HeadItem->Destroy();

			}
			HeadItem = armor;
			SK_HeadMesh->SetSkeletalMesh(HeadItem->interactableMesh->SkeletalMesh);
			break;

		case ESlotType::Torso:

			if (TorsoItem != nullptr) {
				TorsoItem->Destroy();
			}
			TorsoItem = armor;
			SK_TorsoMesh->SetSkeletalMesh(TorsoItem->interactableMesh->SkeletalMesh);
			break;

		case ESlotType::Arms:
			if (RightArmItem != nullptr) {
				RightArmItem->Destroy();
			}

			if (LeftArmItem != nullptr) {
				LeftArmItem->Destroy();
			}

			RightArmItem = armor;
			armor->AttachToActor(this, FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.RightAttachPoint);
			armor->SetActorEnableCollision(false);

			GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, armor->armorInfo.RightAttachPoint.ToString());
			GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, armor->GetName());


			LeftArmItem = armor;
			armor->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.LeftAttachPoint);
			armor->SetActorEnableCollision(false);

			//ArmsItem = armor;
			//SK_ArmsMesh->SetSkeletalMesh(ArmsItem->interactableMesh->SkeletalMesh);
			break;

		case ESlotType::Legs:
			if (LegsItem != nullptr) {
				LegsItem->Destroy();
			}
			LegsItem = armor;
			SK_LegsMesh->SetSkeletalMesh(LegsItem->interactableMesh->SkeletalMesh);
			break;

		case ESlotType::Feet:
			if (FeetItem != nullptr) {
				FeetItem->Destroy();
			}
			FeetItem = armor;
			SK_FeetMesh->SetSkeletalMesh(FeetItem->interactableMesh->SkeletalMesh);
			break;


		case  ESlotType::PrimaryLeftHand:

			if (PrimaryLeftHandItem != nullptr) {//Destroy previously referenced armor attached to socket
				PrimaryLeftHandItem->Destroy();
			}
			PrimaryLeftHandItem = armor;
			PrimaryLeftHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.RightAttachPoint);
			PrimaryLeftHandItem->SetActorEnableCollision(false);
			break;

		case ESlotType::PrimaryRightHand:
			if (PrimaryRightHandItem != nullptr) {
				PrimaryRightHandItem->Destroy();
			}
			PrimaryRightHandItem = armor;
			PrimaryRightHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.LeftAttachPoint);
			PrimaryRightHandItem->SetActorEnableCollision(false);
			break;

		case  ESlotType::SecondaryLeftHand:
			if (SecondaryLeftHandItem != nullptr) {
				SecondaryLeftHandItem->Destroy();
			}
			SecondaryLeftHandItem = armor;
			SecondaryLeftHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.AttachPoint);
			SecondaryLeftHandItem->SetActorEnableCollision(false);
			break;

		case  ESlotType::SecondaryRightHand:
			if (SecondaryRightHandItem != nullptr) {
				SecondaryRightHandItem->Destroy();
			}
			SecondaryRightHandItem = armor;
			SecondaryRightHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, armor->armorInfo.AttachPoint);
			SecondaryRightHandItem->SetActorEnableCollision(false);
			break;
		}//End inner switch for Slot type of item(armor)
		armor->SetActorHiddenInGame(true);
	}//End switch for item type
}

void AYasuosQuestsCharacter::UnAttachEquipment(FItemInfoStruct itemInfo) {

	switch (itemInfo.itemSlot) {

	case ESlotType::Head:

		if (HeadItem != nullptr) {
			HeadItem->Destroy();
			SK_HeadMesh->SetSkeletalMesh(nullptr);
		}
		break;

	case ESlotType::Torso:
		if (TorsoItem != nullptr) {
			TorsoItem->Destroy();
			SK_TorsoMesh->SetSkeletalMesh(nullptr);
		}
		break;

	case ESlotType::Arms:
		if (RightArmItem != nullptr) {
			RightArmItem->Destroy();
			//	SK_ArmsMesh->SetSkeletalMesh(nullptr);

		}

		if (LeftArmItem != nullptr) {
			LeftArmItem->Destroy();
			//	SK_ArmsMesh->SetSkeletalMesh(nullptr);

		}
		break;

	case ESlotType::Legs:
		if (LegsItem != nullptr) {
			LegsItem->Destroy();
			SK_LegsMesh->SetSkeletalMesh(nullptr);

		}
		break;

	case ESlotType::Feet:
		if (FeetItem != nullptr) {
			FeetItem->Destroy();
			SK_FeetMesh->SetSkeletalMesh(nullptr);

		}
		break;

	case ESlotType::PrimaryLeftHand:
		if (PrimaryLeftHandItem != nullptr) {
			PrimaryLeftHandItem->Destroy();

		}
		break;

	case ESlotType::PrimaryRightHand:
		if (PrimaryRightHandItem != nullptr) {
			PrimaryRightHandItem->Destroy();

		}

		break;

	case ESlotType::SecondaryLeftHand:
		if (SecondaryLeftHandItem != nullptr) {
			SecondaryLeftHandItem->Destroy();
		}
		break;

	case ESlotType::SecondaryRightHand:
		if (SecondaryRightHandItem != nullptr) {
			SecondaryRightHandItem->Destroy();

		}
		break;

	case ESlotType::None:
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, "No equipment");
		break;
	}
}

void AYasuosQuestsCharacter::PassReference(ACharacterRecorder* reference) {

	recorderReference = reference;
}


void AYasuosQuestsCharacter::EquipItem(FItemInfoStruct item) {

}

void AYasuosQuestsCharacter::UnEquipItem(FItemInfoStruct item) {

}

//------------------------------------------------------------------------------------------------------------------------------------------------------


void AYasuosQuestsCharacter::Interact(APawn* instig) {

	if (highLightedInteractable) {

		highLightedInteractable->Interact(instig);
	}

}


UHudWidget* AYasuosQuestsCharacter::GetHud() {


	return hudWidget;
}


void AYasuosQuestsCharacter::OnStartJump()
{
	if (IsControlable)
	{
		bPressedJump = true;
		JumpKeyHoldTime = 0.0f;
	}
}

void AYasuosQuestsCharacter::OnStopJumping()
{
	if (IsControlable)
	{
		bPressedJump = false;
		JumpKeyHoldTime = 0.0f;
	}
}


void AYasuosQuestsCharacter::Run() {

	UCharacterMovementComponent* cmc = GetCharacterMovement();
	if (cmc) cmc->MaxWalkSpeed = runSpeed;
}


void AYasuosQuestsCharacter::Walk() {

	UCharacterMovementComponent* cmc = GetCharacterMovement();
	if (cmc) cmc->MaxWalkSpeed = walkSpeed;
}
//--------------------------------------------------------------------- Attack behaviors ------------------------------------------------------------------



void AYasuosQuestsCharacter::ResetCombo() {

	comboCounter = 0;
	isSavingAttack = false;
	bIsAttacking = false;

	ABaseWeapon* weapon = Cast<ABaseWeapon>(PrimaryRightHandItem);
	if (weapon) {
		weapon->ResetAttack();
	}

}


void AYasuosQuestsCharacter::SaveComboAttack() {

	if (isSavingAttack) {//Did we push the attack combo button again before we got here?
						 //If so, we perform combo attack
		isSavingAttack = false;
		UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
		if (comboCounter < attackAnims.Num()) {

			UAnimMontage* attackAnim = attackAnims[comboCounter++];
			if (attackAnim) {

				animInstance->Montage_Play(attackAnim);

			}
		}


	}
}


void AYasuosQuestsCharacter::PerformAttack() {

	if (HasWeaponAttached()) {

		if (!HasWeaponSheathed()) {

			if (!bIsAttacking) {

				bIsAttacking = true;
				UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
				if (comboCounter >= attackAnims.Num()) {

					comboCounter = 0;
				}

				UAnimMontage* attackAnim = attackAnims[comboCounter++];
				if (attackAnim) {

					animInstance->Montage_Play(attackAnim);

				}
			}
			else { //If we are attacking when we performAttack() again, then we are performing a combo attack

				isSavingAttack = true;
			}
		}
	}
}

bool AYasuosQuestsCharacter::GetIsSavingAttack() {

	return isSavingAttack;
}

void AYasuosQuestsCharacter::Attack() {

	ABaseWeapon* weapon = Cast<ABaseWeapon>(PrimaryRightHandItem);
	if (weapon) {
		weapon->Attack();
	}
}



//just make sure to stop the attack animations, and so as the triggers effect
void AYasuosQuestsCharacter::OnPreAttack()
{

	/*if (currentWeapon) {

	currentWeapon->EnableCollisions(true);
	FString message = TEXT(">>>>>>>>>>> Collisions enabled <<<<<<<<<<<< ");
	} else {

	leftHandTrigger->bGenerateOverlapEvents = 1;
	rightHandTrigger->bGenerateOverlapEvents = 1;

	}
	*/
	FString message = TEXT(">>>>>>>>>>> Attack started <<<<<<<<<<<< ");
	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, message);
}

void AYasuosQuestsCharacter::OnPostAttack()
{
	/*if (currentWeapon) {

	currentWeapon->EnableCollisions(false);
	} else {

	leftHandTrigger->bGenerateOverlapEvents = 0;
	rightHandTrigger->bGenerateOverlapEvents = 0;

	}

	*/
	//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Blue, TEXT("Doing Post Attack"));
	attackTimer = 0;

}
void AYasuosQuestsCharacter::ToggleWeapon() {

	if (PrimaryRightHandItem) {
		if (HasWeaponSheathed()) {
			UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
			animInstance->Montage_Play(Draw_Sword_Montage);

		}
		else {
			UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
			animInstance->Montage_Play(Sheath_Sword_Montage);
		}
	}
}


bool AYasuosQuestsCharacter::HasWeaponAttached() {

	return isWeaponAttached;
}

void AYasuosQuestsCharacter::SetHasWeaponAttached(bool val) {

	isWeaponAttached = val;
}

bool AYasuosQuestsCharacter::HasWeaponSheathed() {

	return isWeaponSheathed;
}

void AYasuosQuestsCharacter::SetHasWeaponSheathed(bool val) {

	isWeaponSheathed = val;

}

void AYasuosQuestsCharacter::DrawWeapon() {


	ABaseWeapon* weapon = Cast<ABaseWeapon>(PrimaryRightHandItem);
	if (weapon) {

		PrimaryRightHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.PrimaryRightHand);
		SetHasWeaponSheathed(false);
		SetHasWeaponAttached(true);
		bUseControllerRotationYaw = true;
	}
}

void AYasuosQuestsCharacter::SheathWeapon() {

	ABaseWeapon* weapon = Cast<ABaseWeapon>(PrimaryRightHandItem);
	if (weapon) {

		PrimaryRightHandItem->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->weaponInfo.SecondaryRightHand);
		SetHasWeaponSheathed(true);
		SetHasWeaponAttached(false);
		bUseControllerRotationYaw = false;
	}
}


void AYasuosQuestsCharacter::TurnAtRate(float Rate)
{
	if (IsControlable)
	{
		//calculate delta for this frame from the rate information
		AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
	}
}

void AYasuosQuestsCharacter::LookUpAtRate(float Rate)
{
	if (IsControlable)
	{
		//calculate delta for this frame from the rate information
		AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
	}
}

void AYasuosQuestsCharacter::OnCollectPickup()
{

}

void AYasuosQuestsCharacter::OnSetPlayerController(bool status)
{
	IsControlable = status;
}


void AYasuosQuestsCharacter::OnPostHit() {

	isHit = false;
	EnableInput(Cast<APlayerController>(Controller));

}

FCharacterInfo AYasuosQuestsCharacter::GetCharacterInfo() {

	return characterInfo;
}


float AYasuosQuestsCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) {

	ChangeHealth(Damage);

	if (bIsAttacking) {
		bIsAttacking = false;
	}

	if (Damage < 0) { //Not healing damage

		DisableInput(Cast<APlayerController>(Controller));
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, "damage applied " + FString::SanitizeFloat(Damage));

		if (characterInfo.health <= 0) {
			Die();
		}
		else {

			UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
			animInstance->Montage_Play(takeHitAnim);
		}

	}
	else {

		if (healAnim) {

			UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
			animInstance->Montage_Play(healAnim);
		}
	}




	return Damage;

}


void AYasuosQuestsCharacter::OnPlayerHit(float amount)
{
	//Currently not calling this function
	isHit = true;

	FOutputDeviceNull ar;
	//this->CallFunctionByNameWithArguments(TEXT("ApplyGetDamageEffect"), ar, NULL, true);


	//A message to be printed to screen
	FString message;
	message = TEXT("The health been Saved ---> ") + FString::SanitizeFloat(characterInfo.health);
	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, message);

}


float AYasuosQuestsCharacter::GetHealth() const {
	return characterInfo.health;
}

float AYasuosQuestsCharacter::GetMaxHealth() const {

	return characterInfo.maxHealth;
}

void AYasuosQuestsCharacter::SetHealth(float amount) {
	characterInfo.health = amount;
}

void AYasuosQuestsCharacter::ChangeHealth(float amount) {

	int new_health = GetHealth() + amount;
	if (new_health > characterInfo.maxHealth) {
		new_health = characterInfo.maxHealth;
	}

	if (new_health < 0.f) {

		new_health = 0.f;
	}
	SetHealth(new_health);
	UpdateUserInterfaceBar("Health", new_health / characterInfo.maxHealth);

}

void AYasuosQuestsCharacter::UpdateUserInterfaceBar_Implementation(FName id, float percent) {


}


float AYasuosQuestsCharacter::GetMana() const {
	return characterInfo.mana;
}

float AYasuosQuestsCharacter::GetMaxMana() const {

	return characterInfo.maxMana;
}

void AYasuosQuestsCharacter::SetMana(float amount) {
	characterInfo.mana = amount;
}

void AYasuosQuestsCharacter::AddMana(float amount) {

	float mana = GetMana();
	if (mana < characterInfo.maxMana) {//Since regen happens on tick, check if we have max amount, so we don't process this block.

		int new_mana = mana + amount;
		if (new_mana > characterInfo.maxMana) {
			new_mana = characterInfo.maxMana;
		}

		if (new_mana < 0.f) {

			new_mana = 0.f;
		}

		SetMana(new_mana);
		UpdateUserInterfaceBar("Mana", new_mana / characterInfo.maxMana);
	}
}

void AYasuosQuestsCharacter::AddExp(float xp) {

	characterInfo.experience += xp;
	if (characterInfo.experience >= characterInfo.neededExperience) {

		LevelUP();
	}

}

void AYasuosQuestsCharacter::AddSkillPoint(int point) {

	characterInfo.skillPoints += point;

}


void AYasuosQuestsCharacter::LevelUP() {

	characterInfo.experience = characterInfo.experience - characterInfo.neededExperience;
	characterInfo.playerLevel++;
	AddSkillPoint(1);
	characterInfo.neededExperience = characterInfo.neededExperience * 2;
}

int AYasuosQuestsCharacter::GetExp() const {
	return characterInfo.experience;
}

int AYasuosQuestsCharacter::GetLevel() const {
	return characterInfo.mana;
}

void AYasuosQuestsCharacter::Die() {

	characterInfo.health = 0;
	if (InputEnabled()) {
		DisableInput(Cast<APlayerController>(Controller));
	}

	isAlive = false;
	UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
	animInstance->Montage_Play(dieAnim);

	GetWorld()->GetTimerManager().SetTimer(deathTimerHandle, this, &AYasuosQuestsCharacter::HandleDie, 5.f, false);

}

void AYasuosQuestsCharacter::HandleDie() {

	Destroy();
}

void AYasuosQuestsCharacter::MoveForward(float Value)
{

	forward_movement = Value;

	if ((Controller != NULL) && (Value != 0.0f) && IsControlable)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);

	}
}

void AYasuosQuestsCharacter::MoveRight(float Value)
{

	right_movement = Value;

	if ((Controller != NULL) && (Value != 0.0f) && IsControlable)
	{
		//find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		//get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		//add movement in that direction
		AddMovementInput(Direction, Value);

	}
}


void AYasuosQuestsCharacter::ResetCamera() {

	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator meshRotation = GetMesh()->GetComponentRotation();

	const FRotator YawRotation(Rotation.Pitch, meshRotation.Yaw + 90.f, Rotation.Roll);//Offset roation by 90degrees to face the camera behind the player instead of the right of the player
	Controller->SetControlRotation(YawRotation);
}

void AYasuosQuestsCharacter::AdjustCamera() {



	if (Controller) {
		APlayerController* pc = Cast<APlayerController>(Controller);
		if (pc) {


			// find out which way is forward
			const FRotator ControllerRotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, ControllerRotation.Yaw, 0);

			// get forward vector
			const FVector YawDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

			//get right vector 
			const FVector YawRightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

			//Get vector of movement in world coordinates
			FVector moveResult = ((YawDirection * forward_movement) + (YawRightDirection * right_movement)).GetSafeNormal();

			//The angle between the camera and the vector of movement, perpendicular to the camera or lined up with it
			float cameraAngleFromMovement = FMath::Pow((1 - FMath::Abs(FVector::DotProduct(YawDirection, moveResult))), CameraAnglePower);

			FRotator moveResultRotation = FRotationMatrix::MakeFromX(moveResult).Rotator();
			FRotator deltaRotation = UKismetMathLibrary::NormalizedDeltaRotator(ControllerRotation, moveResultRotation);

			//Length of input vector, when standing still nothing happens
			float inputVectorLength = FMath::Clamp(FMath::Abs(forward_movement) + FMath::Abs(right_movement), 0.f, 1.f);
			float result = FMath::Clamp(GetWorld()->GetDeltaSeconds() * inputVectorLength, 0.f, 1.f);

			AddControllerYawInput(deltaRotation.Yaw * result * CameraRotationRate * cameraAngleFromMovement);
		}
	}
}
#undef LOCTEXT_NAMESPACE 
