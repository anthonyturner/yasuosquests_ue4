// Copyright 2017 @Anthony Turner

#pragma once

#include "Interactables/Pickups/PickupItem.h"
#include "BaseArmor.generated.h"


USTRUCT(Blueprintable)
struct FArmorInfoStruct
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		float DamageRating;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		FName AttachPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		FName RightAttachPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		FName LeftAttachPoint;


	//default constructor
	FArmorInfoStruct()
	{


	}
};

UCLASS()
class YASUOSQUESTS_API ABaseArmor : public APickupItem
{
	GENERATED_BODY()
	
public:
	ABaseArmor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ArmorAttributes)
	FArmorInfoStruct armorInfo;
};
