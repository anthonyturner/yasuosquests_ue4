// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "YasuosQuestsGameMode.generated.h"

UCLASS(minimalapi)
class AYasuosQuestsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AYasuosQuestsGameMode();
};



