// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "IAttack.h"


// This function does not need to be modified.
UIAttack::UIAttack(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IIAttack functions that are not pure virtual.
