// Copyright 2017 @Anthony Turner

#pragma once

#include "Blueprint/UserWidget.h"
#include "EquipmentSlotWidget.h"
#include "EquipmentWidget.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API UEquipmentWidget : public UUserWidget{
	GENERATED_BODY()
	
	
public:


	//void SetEquipmentSlot(FItemInfoStruct itemInfo, UEquipmentSlotWidget* slot);
	int equipmentInventorySize;

	UFUNCTION(BlueprintCallable, Category = Inventory)
		void CreateEquipment();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = UIEquipmentInventory)
		void OnEquipSlot(FItemInfoStruct itemInfo);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = UIEquipmentInventory)
		void OnUpdateEquipmentSlot(FItemInfoStruct itemInfo);

};
