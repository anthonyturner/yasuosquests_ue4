// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "EquipmentWidget.h"
#include "Characters/Playable/YasuosQuestsCharacter.h"



void UEquipmentWidget::OnUpdateEquipmentSlot_Implementation(FItemInfoStruct itemInfo)
{
	OnUpdateEquipmentSlot(itemInfo);


}

void UEquipmentWidget::OnEquipSlot_Implementation(FItemInfoStruct itemInfo)
{
	//add code here

}



void UEquipmentWidget::CreateEquipment() {


	ACharacter* character = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (character) {
		AYasuosQuestsCharacter* player = Cast<AYasuosQuestsCharacter>(character);
		if (player) {

			TArray<FItemInfoStruct> equipmentInventory = player->GetEquipmentInventory();//Pass by reference instead of getting character repeatedly
		
			for (int i = 0; i < equipmentInventory.Num(); i++) {
				
				FItemInfoStruct itemInfo = equipmentInventory[i];
				itemInfo.itemIndex = i;//Update items index to reflect equipment inventory index the item will be stored at.
				OnEquipSlot(itemInfo);//Calls the UI/Blueprint function
			}//End for
		}
	}
}
