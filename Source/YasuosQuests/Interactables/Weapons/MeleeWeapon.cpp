// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "MeleeWeapon.h"



AMeleeWeapon::AMeleeWeapon() {

	
}


void AMeleeWeapon::ResetAttack() {
	damagedActor = nullptr;
}

void AMeleeWeapon::Attack() {

	
	Super::Attack();
	AActor*weaponOwner = GetOwner();
	if (weaponOwner) {

		AYasuosQuestsCharacter* yqPlayer = Cast<AYasuosQuestsCharacter>(weaponOwner);
		if (yqPlayer) {

			const FVector start(weaponOwner->GetActorLocation());
			const FVector endLocation((weaponOwner->GetActorLocation() + (weaponOwner->GetActorForwardVector() * weaponInfo.Range)));
			UWorld* world = GetWorld();
			const FName TraceTag("MyTraceTag");
			world->DebugDrawTraceTag = TraceTag;
			FCollisionQueryParams CollisionParams;
			CollisionParams.TraceTag = TraceTag;
			CollisionParams.AddIgnoredActor(weaponOwner);
			TArray<FHitResult>results;

			bool hasHit = world->LineTraceMultiByChannel(results, start, endLocation, ECollisionChannel::ECC_Camera, CollisionParams);
			if (hasHit) {
				for (FHitResult hitResult : results) {

					// Create a damage event
					TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
					FDamageEvent DamageEvent(ValidDamageTypeClass);
					AActor* actor = hitResult.GetActor();
					weaponInfo.DamageType = EDamageType::Standard;//TODO:: set this based on attack damage
					if (actor) {

						if (damagedActor && actor->IsA(damagedActor->GetClass())) {
							//	return;
						}
						damagedActor = actor;
						ABaseEnemy* enemy = Cast<ABaseEnemy>(actor);
						if (enemy && enemy->IsAlive()) {

							if (weaponInfo.HitVFX)
								UGameplayStatics::SpawnEmitterAtLocation(world, weaponInfo.HitVFX, hitResult.Location);

							if (weaponInfo.HitSFX)
								UGameplayStatics::PlaySoundAtLocation(world, weaponInfo.HitSFX, hitResult.Location);

							float weapon_damage = FMath::RandRange(weaponInfo.DamageMin, weaponInfo.DamageMax);
							float damage = weapon_damage + yqPlayer->GetCharacterInfo().attackDamage;
							enemy->CustomTakeDamage(-damage, this, weaponInfo.DamageType);
						}
					}
				}
			}
		}
	
	}
}

