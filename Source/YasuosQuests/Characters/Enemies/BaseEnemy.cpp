// Fill out your copyright notice in the Description page of Project Settings.

#include "YasuosQuests.h"
#include "BaseEnemy.h"
#include "Perception/PawnSensingComponent.h"
#include "Interfaces/IEnemyState.h"
#include "Kismet/KismetMathLibrary.h"
#include "Animation/AnimInstance.h"


// Sets default values
ABaseEnemy::ABaseEnemy()
{
	

	rangeSphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("rangeSphereCollision"));
	//RangeSphereComp->SetSphereRadius(PawnSensingComp->SightRadius);
	rangeSphereCollision->SetupAttachment(RootComponent);
	rangeSphereCollision->bGenerateOverlapEvents = true;
	rangeSphereCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	rangeSphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	//rangeSphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ABaseEnemy::RangeSphereBeginOverlap);
	rangeSphereCollision->OnComponentEndOverlap.AddDynamic(this, &ABaseEnemy::OnPlayerNotSeen);

	//RangeSphereComp->SetupAttachment(RootComponent);


	//Tick needed or no, it's a defualt from UE
	PrimaryActorTick.bCanEverTick = false;

	//Set the AI Controller class.
	AIControllerClass = AEnemyAIController::StaticClass();

	//Set the enemy behavior values
	SetCurrentHealth(100);
	
	IsDead = false;
	bIsAttacking = false;

	//Because the enmy have to rotate to face the running direction, The Yaw rotation needed!
	bUseControllerRotationYaw = true;

	//Just make sure to not use the Enemy::Character capsul as the navigation collision, and use the agent and set it's radius to something fits the enemy size
	//the main goal is to avoid as much as possible the cases when the enemy meshes intersecting with eachother, or with the environment
	GetCharacterMovement()->NavAgentProps.AgentRadius = 400.f;
	GetCharacterMovement()->SetUpdateNavAgentWithOwnersCollisions(false);

	/*Build the sensing component, and set the required values for it.You can publicate some
		variables to control those values, but for my AI within this game, it is fine to just set some base values here */

	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
	PawnSensingComp->SensingInterval = .25f; // 4 times per second
	PawnSensingComp->bOnlySensePlayers = true;
	PawnSensingComp->SetPeripheralVisionAngle(85.f);
	


	// Create an audio component, the audio component wraps the Cue, 
	// and allows us to ineract with
	// it, and its parameters from code.
	enemyAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("PropellerAudioComp"));
	// I don't want the sound playing the moment it's created.
	enemyAudioComponent->bAutoActivate = false;
	// I want the sound to follow the pawn around, so I attach it to the Pawns root.
	//enemyAudioComponent->SetupAttachment(RootComponent);
	// I want the sound to come from slighty in front of the pawn.
	enemyAudioComponent->SetRelativeLocation(FVector(100.0f, 0.0f, 0.0f));

	lootTableComponent = CreateDefaultSubobject<ULootComponent>(TEXT("LootComponent"));

	//currentState = NewObject <APatrolState>();
}

/*The post initialize required here in order to registering the delegate functions for seeing and hearing
, Those functions came from "Perception/PawnSensingComponent.h" */
void ABaseEnemy::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (PawnSensingComp) {

		PawnSensingComp->OnSeePawn.AddDynamic(this, &ABaseEnemy::OnPlayerSeen);
		
		PawnSensingComp->OnHearNoise.AddDynamic(this, &ABaseEnemy::OnHearNoise);
	}


	if (enemyInfo.standardHitAnim) {

		enemyInfo.currentHitAnim = enemyInfo.standardHitAnim;
	}
	else {

		UE_LOG(LogClass, Log, TEXT("===========>>>>Enemy takeHitAnim not set/found"));

	}

	rangeSphereCollision->SetCollisionObjectType(ECollisionChannel::ECC_PhysicsBody);
	GetMesh()->SetCollisionObjectType(ECollisionChannel::ECC_Destructible);
}


void ABaseEnemy::AddLootItem(FItemInfoStruct item) {

	lootTableComponent->lootTable.Add(item);

}


// Add default functionality here for any IIInteractable functions that are not pure virtual.
void ABaseEnemy::RegisterInteraction(IIInteractable* interact) {

}

void ABaseEnemy::UnRegisterInteraction(IIInteractable* interact) {


}


void ABaseEnemy::Interact(APawn* instig) {

}

// Called when the game starts or when spawned
void ABaseEnemy::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemyTargetPoint::StaticClass(), waypoints);

	rangeSphereCollision->SetSphereRadius(PawnSensingComp->SightRadius);


}

// Called every frame
void ABaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ABaseEnemy::RangeSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {

	AYasuosQuestsCharacter* player = Cast<AYasuosQuestsCharacter>(OtherActor);
	if (player && player->isAlive) {
		SetTarget(player);
	}
}


void ABaseEnemy::OnPlayerSeen(APawn* pawn) {

	AYasuosQuestsCharacter* player = Cast<AYasuosQuestsCharacter>(pawn);
	if (player && player->isAlive) {
		SetTarget(player);
	}
} 


void ABaseEnemy::OnPlayerNotSeen(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	AYasuosQuestsCharacter* player = Cast<AYasuosQuestsCharacter>(OtherActor);
	if (player) {
		SetTarget(nullptr);
	}
}



bool ABaseEnemy::IsAlive() {

	return !IsDead;
}


APawn* ABaseEnemy::GetTarget() {

	return target;
}

void ABaseEnemy::SetTarget(APawn* pawn) {

	target = pawn;
}


float ABaseEnemy::GetAttackRange() {

	return  enemyInfo.AttackRange;
}

float ABaseEnemy::GetChaseDistance() {
	
	return 600.f;

}


bool ABaseEnemy::IsInChaseDistance(APawn* targetObj) {

	float chase_distance = this->GetDistanceTo(targetObj);
	return chase_distance <= 600.f;
}

float ABaseEnemy::GetSightRadius() {

	return 600.f;
}

bool ABaseEnemy::IsInAttackDistance(APawn* targetObj) {

	float attack_distance = this->GetDistanceTo(targetObj);
	//return attack_distance <= AttackRangeSphere->GetUnscaledSphereRadius();
	return attack_distance <= 200.f;
}

// Called to bind functionality to input
void ABaseEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
}

bool ABaseEnemy::HasReachedDestination(FVector location) {

	float distance = FVector::Dist(location, GetActorLocation());
	if (distance <= 1000.f) { //keep moving to location

		return true;
	}
	else {

		return false;
	}
}



void ABaseEnemy::Walk() {

	
	UCharacterMovementComponent* cmv = GetCharacterMovement();
	if (cmv) GetCharacterMovement()->MaxWalkSpeed = walkSpeed;
}


void ABaseEnemy::Run() {
	UCharacterMovementComponent* cmv = GetCharacterMovement();
	if (cmv) GetCharacterMovement()->MaxWalkSpeed = runSpeed;
}





void ABaseEnemy::FaceRotation(FRotator NewRotation, float DeltaTime)
{
	FRotator CurrentRotation = FMath::RInterpTo(GetActorRotation(), NewRotation, DeltaTime, 8.0f);
	Super::FaceRotation(CurrentRotation, DeltaTime);
}



void ABaseEnemy::PerformAttack(FVector attackLocation) {

	if (!bIsAttacking) {

		bIsAttacking = true;
		
		FRotator lookRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), attackLocation);
		const FRotator YawRotation(0, lookRotation.Yaw, 0);

		SetActorRotation(YawRotation);

		if (comboCounter >= attackAnims.Num()) {

			comboCounter = 0;
		}

		UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
		UAnimMontage* attackAnim = attackAnims[comboCounter++];
		if (attackAnim) {

			animInstance->Montage_Play(attackAnim);

		}
	}
	else { //If we are attacking when we performAttack() again, then we are performing a combo attack

		isSavingAttack = true;
	}
}


void ABaseEnemy::ResetCombo() {

	comboCounter = 0;
	isSavingAttack = false;
	bIsAttacking = false;
	/*ABaseWeapon* weapon = Cast<ABaseWeapon>(PrimaryRightHandItem);
	if (weapon) {
		weapon->ResetAttack();
	}*/

}


void ABaseEnemy::SaveComboAttack() {

	if (isSavingAttack) {//Did we push the attack combo button again before we got here?
						 //If so, we perform combo attack
		isSavingAttack = false;
		UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
		if (comboCounter < attackAnims.Num()) {

			UAnimMontage* attackAnim = attackAnims[comboCounter++];
			if (attackAnim) {

				animInstance->Montage_Play(attackAnim);
			}
		}
	}
}


//TODO: Update this logic and comment it
void ABaseEnemy::Attack()
{

	if (enemyInfo.AttackSFX && enemyInfo.AttackSFX->IsValidLowLevelFast()) {

		enemyAudioComponent->SetSound(enemyInfo.AttackSFX);
		// Finally play the sound  (outside the constructor)
		enemyAudioComponent->Play();
		// Also... if you want to set a parameter of the sound cue during runtime:
		enemyAudioComponent->SetFloatParameter(FName("pitch"), 2500.f);
	}

	

	//UE_LOG(LogClass, Log, TEXT("===========>>>> Starting Melee Attack"));
	const FVector TraceStart = GetActorLocation();
	const FVector LookingDirection = GetActorRotation().Vector();
	const FVector TraceEnd = TraceStart + LookingDirection * enemyInfo.AttackRange;



	//perform a sphere sweep
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
	FCollisionQueryParams TracingParameters(WeaponFireTag, true);
	TracingParameters.AddIgnoredActor(this);
	TracingParameters.bTraceAsyncScene = true;
	TracingParameters.bReturnPhysicalMaterial = true;


	FHitResult Hit(ForceInit);
	GetWorld()->SweepSingleByChannel(Hit, TraceStart, TraceEnd, FQuat::Identity, ECollisionChannel::ECC_Pawn, FCollisionShape::MakeSphere(25), TracingParameters);

	//if it really hit something
	if (Hit.Actor != NULL)
	{
		AYasuosQuestsCharacter *character = Cast<AYasuosQuestsCharacter>(Hit.GetActor());
		if (character != NULL)
		{
			FString debuggingString = character->GetName();
			//UE_LOG(LogClass, Log, TEXT("The enmey has found ^_^_^_^_^_^_^_^__^_^_^_^__^_^ %s"), *debuggingString);

			FPointDamageEvent PointDmg;
			PointDmg.DamageTypeClass = UDamageType::StaticClass();
			PointDmg.HitInfo = Hit;
			PointDmg.ShotDirection = LookingDirection;
			PointDmg.Damage = GetAttackDamage();
			character->TakeDamage(-GetAttackDamage(), PointDmg, Controller, this);
			if (enemyInfo.HitVFX) {

				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), enemyInfo.HitVFX, Hit.Location);
			
			}
			if (!character->isAlive) {
				SetTarget(nullptr);
			}
		}
	}

	/*if (weapon) {

		weapon->Attack();
	}*/
}

void ABaseEnemy::PlayPlayerLostSound() {

	if (enemyInfo.playerLostSound && enemyInfo.playerLostSound->IsValidLowLevelFast()) {

		enemyAudioComponent->SetSound(enemyInfo.playerLostSound);
		// Finally play the sound  (outside the constructor)
		enemyAudioComponent->Play();
		// Also... if you want to set a parameter of the sound cue during runtime:
		enemyAudioComponent->SetFloatParameter(FName("pitch"), 2500.f);
	}

}


void ABaseEnemy::PlaySpottedSound() {

	if (enemyInfo.playerSpottedSound && enemyInfo.playerSpottedSound->IsValidLowLevelFast()) {

		enemyAudioComponent->SetSound(enemyInfo.playerSpottedSound);
		// Finally play the sound  (outside the constructor)
		enemyAudioComponent->Play();
		// Also... if you want to set a parameter of the sound cue during runtime:
		enemyAudioComponent->SetFloatParameter(FName("pitch"), 2500.f);
	}
}


//The hear noise implementation
void ABaseEnemy::OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume)
{
	const FString VolumeDesc = FString::Printf(TEXT(" at volume %f"), Volume);
	FString message = TEXT(">>>>>>>>>>> Heard Actor <<<<<<<<<<<< ") + OtherActor->GetName() + VolumeDesc;
	//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, message);
	//UE_LOG(LogClass, Log, TEXT(">>>>>>>>>>> Heard Actor <<<<<<<<<<<<"));   
}



void ABaseEnemy::Die() {

	enemyInfo.health = 0;
	IsDead = true;
	UAnimInstance* animInstance = GetMesh()->GetAnimInstance();
	animInstance->Montage_Play(enemyInfo.deathAnim);

	GetWorld()->GetTimerManager().SetTimer(dieTimerHandle, this, &ABaseEnemy::RemoveEnemy, 5.f, false);
	if( lootTableComponent){
		lootTableComponent->CalculateLoot();
	}
}

void ABaseEnemy::DropItems() {


}

void ABaseEnemy::RemoveEnemy() {

	Destroy();
}
void ABaseEnemy::SetCurrentHealth(int amount) {

	enemyInfo.health = amount;
}

void ABaseEnemy::ChangeHealth(int amount) {

	int health = GetCurrentHealth() + amount;
	SetCurrentHealth(health);
}

int ABaseEnemy::GetCurrentHealth() {

	return enemyInfo.health;
}

float ABaseEnemy::GetAttackDamage() {

	return enemyInfo.AttackDamage;

}

float ABaseEnemy::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) {
	
	// Create a damage event
	// Create a damage event  

	return CustomTakeDamage(Damage, DamageCauser, EDamageType::Standard);
	
}

float ABaseEnemy::CustomTakeDamage(float Damage, class AActor* DamageCauser, EDamageType damageType) {

	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, "damage applied " + FString::SanitizeFloat(Damage));
	RecentDamageTaken = Damage;
	//GetWorld()->GetTimerManager().SetTimer(damageTakenTimerHandle, this, &ABaseEnemy::ResetDamageTaken, 3.f, false);

	FVector damageCauserLocation = DamageCauser->GetActorLocation();

	//float dot = FVector::DotProduct(GetActorLocation()->, (MouseHit - Pawn->Location).Normalize())	
	float dot = FMath::RadiansToDegrees(acos(FVector::DotProduct(GetActorLocation().ForwardVector.GetSafeNormal(), (DamageCauser->GetActorLocation() - GetActorLocation()).GetSafeNormal())));
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, "weapon angle " + FString::SanitizeFloat(dot));
	
	//isHit = true;
	switch (damageType) {

	case EDamageType::Standard:

		if (dot >= 70.f && dot <= 180.f) {
			enemyInfo.currentHitAnim = enemyInfo.standardHitAnim;
		}
		else {

			enemyInfo.currentHitAnim = enemyInfo.backHitAnim;

		}
		break;
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, "damage applied " + FString::SanitizeFloat(Damage));

	case EDamageType::KnockBack:
		enemyInfo.currentHitAnim = enemyInfo.knockBackHitAnim;
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, "knock back " + FString::SanitizeFloat(Damage));

		break;

	case EDamageType::KnockDown:
		enemyInfo.currentHitAnim = enemyInfo.knockDownHitAnim;
		break;

	case EDamageType::Launch:
		enemyInfo.currentHitAnim = enemyInfo.lauchHitAnim;
		break;
	}

	UAnimInstance* animInstance = GetMesh()->GetAnimInstance();

	if (bIsAttacking) {

		bIsAttacking = false;

	}
	for (int i = 0; i < attackAnims.Num(); i++) {

		UAnimMontage* attackAnim = attackAnims[i];
		if (animInstance->Montage_IsPlaying(attackAnim)) {

			animInstance->Montage_Stop(0.f, attackAnim);
			break;
		}

	}
	
	animInstance->Montage_Play(enemyInfo.currentHitAnim);
	ChangeHealth(Damage);
	if (GetCurrentHealth() <= 0) {
		Die();
	}

	return Damage;
}


void ABaseEnemy::OnPostHit() {

	isHit = false;
}

void ABaseEnemy::OnEnemyHit(float amount)
{

	isHit = true;
	ChangeHealth(-amount);
	if (GetCurrentHealth() <= 0) {
		Die();
	}

	FOutputDeviceNull ar;
	//this->CallFunctionByNameWithArguments(TEXT("ApplyGetDamageEffect"), ar, NULL, true);


	//A message to be printed to screen
	FString message;
	message = TEXT("Enemy health ") + FString::SanitizeFloat(GetCurrentHealth());
	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, message);
	/*
	//Creating an instance of the bellz save class
	UYQSaveGame* YQSaveGameInstance = Cast<UYQSaveGame>(UGameplayStatics::CreateSaveGameObject(UYQSaveGame::StaticClass()));
	//Change the health value of the save class to match the current health value of the gladiator
	YQSaveGameInstance->PlayerHealth = TotalHealth;
	//Store the health value to the save file
	UGameplayStatics::SaveGameToSlot(YQSaveGameInstance, YQSaveGameInstance->SlotName, YQSaveGameInstance->PlayerIndex);
	//update the message that will be printed
	message = TEXT("The health been Saved ---> ") + FString::SanitizeFloat(TotalHealth);
	//print a message to the screen
	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, message);

	*/
}


bool ABaseEnemy::IsHit() {

	return isHit;
}


void ABaseEnemy::SetHit(bool hit) {

	isHit = hit;
}



void ABaseEnemy::SetAttacking(bool attacking) {

	bIsAttacking = attacking;
	

}

bool ABaseEnemy::IsAttacking() {

	return bIsAttacking;
}